//
//  FAUserProfileViewController.h
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Person;

@interface FAUserProfileViewController : UITableViewController

@property (nonatomic, strong) Person *          person;

@end
