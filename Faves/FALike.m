//
//  FALike.m
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FALike.h"

@implementation FALike
@dynamic id_like;
@dynamic name;
@dynamic category;
@dynamic id_user;

+ (void)load {
    [self registerSubclass];
    
    [[self object] pinInBackground];
}

+ (NSString *)parseClassName{
    return @"Likes";
}

@end
