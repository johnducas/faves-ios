//
//  FAHomeViewController.h
//  Faves
//
//  Created by Chen Wang on 11/03/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAHomeViewController : UIViewController{
    
    IBOutlet UICollectionView *     colList;
    
    IBOutlet UIView *               viewBottom;
    
    IBOutlet UIButton *             btnNope;
    IBOutlet UIButton *             btnLike;
    
}

- (IBAction)actionNope:(id)sender;
- (IBAction)actionLike:(id)sender;

@end
