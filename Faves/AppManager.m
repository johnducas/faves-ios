//
//  AppManager.m
//  Kwibal
//
//  Created by Chen Wang on 5/2/14.
//  Copyright (c) 2014 Kwibal. All rights reserved.
//

#import "AppManager.h"

@interface AppManager (){
    
    
}

@end

@implementation AppManager

#pragma mark - Shared Manager
+ (AppManager* ) sharedManager{
    static AppManager *_sharedClient = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[AppManager alloc] init];
        _sharedClient.arrayCategory = [NSMutableArray array];
        _sharedClient.arrayLikes = [NSMutableArray array];
        
    });
    
    return _sharedClient;
}

- (id)init{
    
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}

- (void)saveCategory;{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.arrayCategoryID        forKey:@"arrayCategoryID"];
    [defaults setObject:self.arrayCategoryName      forKey:@"arrayCategoryName"];
    
    [defaults synchronize];
}

- (void)loadCategory;{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.arrayCategoryID    = [defaults objectForKey:@"arrayCategoryID"];
    self.arrayCategoryName  = [defaults objectForKey:@"arrayCategoryName"];
    
}

- (BOOL)isCategoryExist : (FACategory *)_category;
{
    for (FACategory *category in self.arrayCategory) {
        if ([category.id_category isEqualToString:_category.id_category]) {
            return YES;
        }
    }
    
    return NO;
    
}

- (BOOL)isLikePageExist : (FALike *)_like;{
    
    for (FALike *like in self.arrayLikes) {
        if ([like.id_like isEqualToString:_like.id_like]) {
            return YES;
        }
    }
    
    return NO;
    
}

#pragma mark - Get URL from Facebook ID
- (NSString *)imageUrlFromFbId : (NSString *) _fbId;{
    
    NSMutableString *string = [NSMutableString string];
    
    [string appendString:@"https://graph.facebook.com/"];
    
    [string appendString:_fbId];
    
    [string appendString:@"/picture?type=normal"];
    
    return string;
    
}


#pragma mark - show Alert
- (void)showAlert : (NSString *)title message : (NSString *)message;{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

@end

NSString * const FETCHLIKES_MINE_COMPLETED              = @"fetch_likes_pages";
