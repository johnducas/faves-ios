//
//  WelcomeViewController.h
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EAIntroView.h>

@interface WelcomeViewController : UIViewController<EAIntroDelegate>{
    
    IBOutlet UIImageView *              imgBack;
    
    IBOutlet UIButton *                 btnFacebook;
    
    
}

- (IBAction)actionFacebook:(id)sender;

@end
