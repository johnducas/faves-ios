//
//  FACategory.h
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FACategory : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *            id_category;
@property (nonatomic, strong) NSString *            name;

+ (NSString *)parseClassName;

@end
