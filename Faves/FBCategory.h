//
//  FBCategory.h
//  Faves
//
//  Created by Miloslav Sorokin on 22/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBCategory : NSObject

@property (nonatomic, strong) NSString *            categoryID;
@property (nonatomic, strong) NSString *            categoryName;

+ ( id ) me;
- ( id ) initWithDict : ( NSDictionary* ) _dict ;

@end
