//
//  FALike.h
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FALike : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *        id_like;
@property (nonatomic, strong) NSString *        id_user;
@property (nonatomic, strong) NSString *        category;
@property (nonatomic, strong) NSString *        name;

+ (NSString *)parseClassName;

@end
