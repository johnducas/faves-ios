//
//  Constant.h
//  EquitySnap
//
//  Created by Chen Wang on 4/18/14.
//  Copyright (c) 2104 Kwibal. All rights reserved.
//

#ifndef Kwibal_Constant_h
#define Kwibal_Constant_h

#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import <Chameleon.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#import <LBBlurredImage/UIImageView+LBBlurredImage.h>

#import "UIColor+Hex.h"
#import "UIButton+Extension.h"
#import "NSString+Extension.h"
#import "UINavigationController+CustomAnimation.h"
#import "UIStoryboard+Extension.h"
#import "UICustomTextField.h"

#import "AppManager.h"
#import "CommunicationManager.h"

#import "FACategory.h"
#import "FAFriend.h"
#import "FALike.h"

#define BASE_URL                                            @"http://192.168.3.63/Faves/"           //beta

#define COLOR_BLUE_UNIMATCH                                 0x3b5999

#define COLOR_BLUE_MAIN                                     0x005ebf
#define COLOR_BLUE_BUBBLE                                   0x12A5F4
#define COLOR_LIKE                                          0x1df56a
#define COLOR_NOPE                                          0xf75b25

//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]


#define FONT_HELVETICA_NEUE                                 @"HelveticaNeue"
#define FONT_HELVETICA_NEUE_BOLD                            @"HelveticaNeue-Bold"
#define FONT_HELVETICA_NEUE_BOLD_ITALIC                     @"HelveticaNeue-BoldItalic"
#define FONT_HELVETICA_NEUE_CONDENSED_BLACK                 @"HelveticaNeue-CondensedBlack"
#define FONT_HELVETICA_NEUE_ITALIC                          @"HelveticaNeue-Italic"
#define FONT_HELVETICA_NEUE_LIGHT                           @"HelveticaNeue-Light"
#define FONT_HELVETICA_NEUE_LIGHT_ITALIC                    @"HelveticaNeue-LightItalic"
#define FONT_HELVETICA_NEUE_MEDIUM                          @"HelveticaNeue-Medium"
#define FONT_HELVETICA_NEUE_MEDIUMITALIC                    @"HelveticaNeue-MediumItalic"
#define FONT_HELVETICA_NEUE_ULTRALIGHT                      @"HelveticaNeue-UltraLight"
#define FONT_HELVETICA_NEUE_ULTRALIGHT_ITALIC               @"HelveticaNeue-UltraLightItalic"
#define FONT_HELVETICA_NEUE_THIN                            @"HelveticaNeue-Thin"
#define FONT_HELVETICA_NEUE_THIN_ITALIC                     @"HelveticaNeue-Thin_Italic"



//live
//#define FB_APP_ID                       @"758685244207893"
//#define FB_APP_SECRET                   @"2eb637a3e66356e934d14e2b60c380f7"

//dev
//#define FB_APP_ID                       @"808312435905771"
//#define FB_APP_SECRET                   @"95934827496ca40b61166db2e2f4030e"

#define FB_APP_ID                       @"794730823930599"
#define FB_APP_SECRET                   @"42b7584973207752037c352823861e5b"

//#define FB_APP_ID                       @"760377910705293"
//#define FB_APP_SECRET                   @"dcca6f19bdb05d39921d76f6743ecc17"
//1527179320891788
//edf0a549b678c510a0d63fe4cf084df1
//#define FB_APP_ID                       @"1527179320891788"
//#define FB_APP_SECRET                   @"edf0a549b678c510a0d63fe4cf084df1"

#define PARSE_APP_ID                    @"6NQElzXS9y6Xy9NxohlYJPT8edMvNFK8Gvhv8Nbu"
#define PARSE_CLIENT_KEY                @"9a5Hn0mDSuD50okzr9eSLwZ7FMdYq9dH205ltOZ6"

#define INSTAGRAM_ID                    @"39a1ac4282a043cd9b04e62c166cc93e"
#define INSTAGRAM_SECRET                @"6bff9d7800c54e20bf27cd3e32509639"
#define INSTAGRAM_URI                   @"http://hongjisoft.com"


#define FB_LOG_IN_NOTIFICATON           @"login"
#define FB_LOG_OUT_NOTIFICATON          @"logout"


#define STORYBOARD_MAIN                 @"Main"
#define STORYBOARD_WELCOME              @"Welcome"
#define STORYBOARD_SLDING               @"Sliding"
#define STORYBOARD_MYPROFILE            @"MyProfile"
#define STORYBOARD_HOME                 @"Home"
#define STORYBOARD_ADDFAVE              @"AddFave"
#define STORYBOARD_SEARCH               @"Search"
#define STORYBOARD_INBOX                @"Inbox"
#define STORYBOARD_MATCHES              @"Matches"
#define STORYBOARD_OTHER                @"Other"


#endif
