//
//  FAFriend.h
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAFriend : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *        id_user;
@property (nonatomic, strong) NSString *        id_friend;

+ (NSString *)parseClassName;

@end
