//
//  FAUserProfileViewController.m
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAUserProfileViewController.h"
#import "ProfileHeaderView.h"
#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>

#import "Person.h"

#import "FAUserProfileFriendsViewCell.h"
#import "FAUserProfileHashtagViewCell.h"
#import "FAUserProfileInterestViewCell.h"
#import "FAUserProfileMoviesViewCell.h"
#import "FAUserProfilePhotoViewCell.h"


#define IDENTIFIER_FRIENDS          @"FAUserProfileFriendsViewCell"
#define IDENTIFIER_HASHTAG          @"FAUserProfileHashtagViewCell"
#define IDENTIFIER_INTEREST         @"FAUserProfileInterestViewCell"
#define IDENTIFIER_MOVIES           @"FAUserProfileMoviesViewCell"
#define IDENTIFIER_PHOTO            @"FAUserProfilePhotoViewCell"

#define SCREEN_WIDTH                ([UIScreen mainScreen].bounds.size.width)


@interface FAUserProfileViewController ()

@end

@implementation FAUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    [self loadPerson];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_FRIENDS bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_FRIENDS];
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_HASHTAG bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_HASHTAG];
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_INTEREST bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_INTEREST];
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_MOVIES bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_MOVIES];
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_PHOTO bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_PHOTO];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
//    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
//    [(UIView*)[self.navigationController.navigationBar.subviews objectAtIndex:0] setAlpha:0.f];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
//    
//    [(UIView*)[self.navigationController.navigationBar.subviews objectAtIndex:0] setAlpha:1.f];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
}


#pragma mark - load Controller
- (void)loadController{
    
    self.view.backgroundColor = FlatWhite;
    
    ProfileHeaderView *headerView = [[ProfileHeaderView alloc] init];
    
    [self.tableView setParallaxHeaderView:headerView
                                     mode:VGParallaxHeaderModeFill // For more modes have a look in UIScrollView+VGParallaxHeader.h
                                   height:250];
}

- (void)loadPerson{
    
    self.navigationItem.title       = self.person.name;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:{
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_PHOTO forIndexPath:indexPath];
            
//            cell = [FAUserProfilePhotoViewCell instantiateFromNib];
            break;
        }
            
        case 1:{
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_HASHTAG forIndexPath:indexPath];
            
            [(FAUserProfileHashtagViewCell *)cell configureCell:nil];
            
            break;
        }
            
        case 2:{
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_FRIENDS forIndexPath:indexPath];
            break;
        }
            
        case 3:{
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_INTEREST forIndexPath:indexPath];
            break;
        }
            
        case 4:{
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_MOVIES forIndexPath:indexPath];
            break;
        }
            
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
        return SCREEN_WIDTH / 2;
        
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_PHOTO forIndexPath:indexPath];
//        
//        return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1;
        
//        FAUserProfilePhotoViewCell *cell = nil;
//        
//        if (!cell) {
//            cell = [FAUserProfilePhotoViewCell instantiateFromNib];
//        }
//        
//        return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1;
        
    }else if (indexPath.section == 1){
        
        FAUserProfileHashtagViewCell *cell = nil;
        
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_HASHTAG];
        }
        
        [cell configureCell:nil];
        
        return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1;
        
    }else if (indexPath.section == 2){
        
        return SCREEN_WIDTH / 4;
    }
    
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 1.f;
    }
    
    return 30.f;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return nil;
    }
    
    UIView *viewHeader = [[UIView alloc] init];
    viewHeader.backgroundColor = FlatGray;
    
    UILabel * lblName = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width - 30, 30)];
    [lblName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:14]];
    [lblName setTextColor:FlatBlack];
    
    switch (section) {
        case 1:{
            lblName.text = @"Hashtags";
            break;
        }
        case 2:{
            lblName.text = @"Mutal Friends";
            break;
        }
        case 3:{
            lblName.text = @"Shared Interest";
            break;
        }
        case 4:{
            lblName.text = @"TV Shows, Movies";
            break;
        }
            
        default:
            break;
    }
    
    [viewHeader addSubview:lblName];
    
    return viewHeader;
}


#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // This must be called in order to work
    [self.tableView shouldPositionParallaxHeader];
    
    // scrollView.parallaxHeader.progress - is progress of current scroll
    NSLog(@"Progress: %f", scrollView.parallaxHeader.progress);
    
    
//    [(UIView*)[self.navigationController.navigationBar.subviews objectAtIndex:0] setAlpha:1-scrollView.parallaxHeader.progress];
    
    // This is how you can implement appearing or disappearing of sticky view
    //    [scrollView.parallaxHeader setAlpha:scrollView.parallaxHeader.progress];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
