//
//  FAUserProfilePhotoViewCell.m
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAUserProfilePhotoViewCell.h"

#import "FAProfilePhotoView.h"

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)

@implementation FAUserProfilePhotoViewCell

+ (instancetype)instantiateFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"%@", [self class]] owner:nil options:nil];
    return [views firstObject];
}


- (void)awakeFromNib {
    // Initialization code
    
    viewPhotos.delegate = self;
    viewPhotos.dataSource = self;
    
    viewPhotos.itemsPerPage = 2;
    viewPhotos.pagingEnabled = YES;
    viewPhotos.truncateFinalPage = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView;{
    return 10;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view;{
    
    if (!view)
    {
        //load new item view instance from nib
        //control events are bound to view controller in nib file
        //note that it is only safe to use the reusingView if we return the same nib for each
        //item view, if different items have different contents, ignore the reusingView value
        view = [FAProfilePhotoView instantiateFromNib];
        
        CGRect rect = view.frame;
        
        rect.size.width = SCREEN_WIDTH / 2 ;
        rect.size.height = SCREEN_WIDTH / 2;
        
        view.frame = rect;
        
    }
    
    return view;
    

}

@end
