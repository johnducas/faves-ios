//
//  FAUserPhotoViewController.m
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAUserPhotoViewController.h"

#import "MNWheelView.h"

@interface FAUserPhotoViewController ()

@end

@implementation FAUserPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    self.navigationItem.title = @"Photos";
    
    UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel:)];
    self.navigationItem.leftBarButtonItem = btnCancel;
    
    self.view.backgroundColor = FlatWhite;
    
    
    MNWheelView *view=[[MNWheelView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 200)];
    
    UIImage *image1=[UIImage imageNamed:@"1"];
    UIImage *image2=[UIImage imageNamed:@"2"];
    UIImage *image3=[UIImage imageNamed:@"3"];
    UIImage *image4=[UIImage imageNamed:@"4"];
    UIImage *image5=[UIImage imageNamed:@"5"];
    
    //        view.imageNames=[NSArray arrayWithObjects:@"a",@"b",@"c",@"d", nil];  view.imageNames 和view.images 二选一
    view.images=[NSArray arrayWithObjects:image1,image2,image3,image4,image5, nil];
    view.backgroundColor=[UIColor clearColor];
    view.click=^(int i)
    {
        NSLog(@"单击了%d",i);
        
    };
    [self.view addSubview:view];
    
}

#pragma mark - Actions
- (void)actionCancel : (id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
