//
//  InboxViewController.m
//  Faves
//
//  Created by Miloslav Sorokin on 12/8/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "InboxViewController.h"
#import "MessageViewController.h"

#import "InboxViewCell.h"

#define IDENTIFIER_CELL                     @"InboxViewCell"

@interface InboxViewController (){
    
    NSMutableArray *                arrayList;
}

@end

@implementation InboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];

    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_CELL bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_CELL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"Inbox"];
    
    
    [self.view setBackgroundColor:FlatWhite];
    
    arrayList = [NSMutableArray array];
    
}

#pragma mark - load Chat History
- (void)loadInbox{
    
    
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return arrayList.count;
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InboxViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_CELL forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MessageViewController *viewController = [MessageViewController new];
    
    [self.navigationController pushViewController:viewController animated:NO];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
