//
//  User.m
//  OneNeed
//
//  Created by Chen Wang on 11/11/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic name;
@dynamic gender;
@dynamic birthday;
@dynamic fbId;

+ (void)load {
    [self registerSubclass];
}

@end
