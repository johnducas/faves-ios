//
//  HomeViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "HomeViewController.h"
#import <YIFullScreenScroll.h>
#import <PulsingHalo/PulsingHaloLayer.h>
#import "Person.h"
#import <MDCSwipeToChoose/MDCSwipeToChoose.h>

#import "FAUserProfileViewController.h"



static const CGFloat ChoosePersonButtonHorizontalPadding        = 60.f;
static const CGFloat ChoosePersonButtonVerticalPadding          = 20.f;
static const CGFloat ChoosePersonButtonWidth                    = 55.f;
static const CGFloat ChoosePersonButtonHeight                   = 55.f;

@interface HomeViewController ()<ChoosePersonViewDelegate>{
    
    NSMutableArray *                arraList;
    PulsingHaloLayer *              halo;
    
    UIImageView *                   imgViewProfile;
    
}

@property (nonatomic, strong) NSMutableArray *people;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _people = [[self defaultPeople] mutableCopy];
    
    
    [self loadController];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.view setBackgroundColor:[UIColor flatWhiteColor]];
    
    UIView *viewTitle = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 44)];
    self.navigationItem.titleView = viewTitle;
    
    UIImageView *imgViewLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    [imgViewLogo setImage:[UIImage imageNamed:@"logo_unimatch"]];
    [imgViewLogo setContentMode:UIViewContentModeScaleAspectFit];
    imgViewLogo.center = viewTitle.center;
    [viewTitle addSubview:imgViewLogo];
    
    UIBarButtonItem *btnMenu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(actionMenu:)];
    [self.navigationItem setRightBarButtonItem:btnMenu];

    
    halo = [PulsingHaloLayer layer];
    halo.position = self.view.center;
    [self.view.layer addSublayer:halo];
    
    halo.radius = 160;
    [halo setBackgroundColor:[UIColor flatBlueColor].CGColor];
    [self showHaloView:YES];
    
    
    [self performSelector:@selector(loadCardViews) withObject:nil afterDelay:3];
    
    imgViewProfile  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [imgViewProfile setCenter:self.view.center];
    [imgViewProfile.layer setCornerRadius:50.f];
    [imgViewProfile.layer setMasksToBounds:YES];
    [imgViewProfile.layer setBorderColor:UIColorFromRGBWithAlpha(COLOR_BLUE_MAIN, .6f).CGColor];
    [imgViewProfile.layer setBorderWidth:1.f];
    [imgViewProfile setImage:[UIImage imageNamed:@"temp1.png"]];
    
    [self.view addSubview:imgViewProfile];
    
}

- (void)loadCardViews{
    
    [self showHaloView : NO];
    
    // Display the first ChoosePersonView in front. Users can swipe to indicate
    // whether they like or dislike the person displayed.
    self.frontCardView = [self popPersonViewWithFrame:[self frontCardViewFrame]];
    self.frontCardView.delegate = self;
    [self.view addSubview:self.frontCardView];
    
    // Display the second ChoosePersonView in back. This view controller uses
    // the MDCSwipeToChooseDelegate protocol methods to update the front and
    // back views after each user swipe.
    self.backCardView = [self popPersonViewWithFrame:[self backCardViewFrame]];
    self.backCardView.delegate = self;
    [self.view insertSubview:self.backCardView belowSubview:self.frontCardView];
    
    // Add buttons to programmatically swipe the view left or right.
    // See the `nopeFrontCardView` and `likeFrontCardView` methods.
    [self constructNopeButton];
    [self constructLikedButton];
    [self constructInfoButton];
    
}

- (void)showHaloView : (BOOL)show{
    
    if (show) {
        halo.hidden = NO;
    }else{
        halo.hidden = YES;
    }
}

#pragma mark - Actions
- (void)actionMenu : (id)sender{

    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeFilterViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
    
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
//    [self presentViewController:navController animated:YES completion:nil];
    
}

- (void)actionAdd : (id)sender{
    
    
}

#pragma mark - ChoosPersonView Delegate
- (void)actionChoosePersonViewTouched : (Person *)person{
    
    NSLog(@"delegate called = %@", person.name);
    
    FAUserProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAUserProfileViewController"];
    viewController.person = person;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

#pragma mark - MDCSwipeToChooseDelegate Protocol Methods

// This is called when a user didn't fully swipe left or right.
- (void)viewDidCancelSwipe:(UIView *)view {
    NSLog(@"You couldn't decide on %@.", self.currentPerson.name);
}

// This is called then a user swipes the view fully left or right.
- (void)view:(UIView *)view wasChosenWithDirection:(MDCSwipeDirection)direction {
    // MDCSwipeToChooseView shows "NOPE" on swipes to the left,
    // and "LIKED" on swipes to the right.
    if (direction == MDCSwipeDirectionLeft) {
        NSLog(@"You noped %@.", self.currentPerson.name);
    } else {
        NSLog(@"You liked %@.", self.currentPerson.name);
    }
    
    // MDCSwipeToChooseView removes the view from the view hierarchy
    // after it is swiped (this behavior can be customized via the
    // MDCSwipeOptions class). Since the front card view is gone, we
    // move the back card to the front, and create a new back card.
    self.frontCardView = self.backCardView;
    if ((self.backCardView = [self popPersonViewWithFrame:[self backCardViewFrame]])) {
        // Fade the back card into view.
        self.backCardView.alpha = 0.f;
        [self.view insertSubview:self.backCardView belowSubview:self.frontCardView];
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.backCardView.alpha = 1.f;
                         } completion:nil];
    }
}

#pragma mark - Internal Methods

- (void)setFrontCardView:(ChoosePersonView *)frontCardView {
    // Keep track of the person currently being chosen.
    // Quick and dirty, just for the purposes of this sample app.
    _frontCardView = frontCardView;
    self.currentPerson = frontCardView.person;
}

- (NSArray *)defaultPeople {
    // It would be trivial to download these from a web service
    // as needed, but for the purposes of this sample app we'll
    // simply store them in memory.
    return @[
             [[Person alloc] initWithName:@"John Ducas"
                                    image:[UIImage imageNamed:@"finn"]
                                      age:16
                    numberOfSharedFriends:60
                  numberOfSharedInterests:14
                           numberOfPhotos:20],
             [[Person alloc] initWithName:@"Chen Wang"
                                    image:[UIImage imageNamed:@"jake"]
                                      age:28
                    numberOfSharedFriends:8
                  numberOfSharedInterests:18
                           numberOfPhotos:8],
             [[Person alloc] initWithName:@"Benny Kao"
                                    image:[UIImage imageNamed:@"fiona"]
                                      age:30
                    numberOfSharedFriends:5
                  numberOfSharedInterests:34
                           numberOfPhotos:5],
             [[Person alloc] initWithName:@"Chuck DeMonte"
                                    image:[UIImage imageNamed:@"prince"]
                                      age:45
                    numberOfSharedFriends:12
                  numberOfSharedInterests:18
                           numberOfPhotos:24],
             ];
}

- (ChoosePersonView *)popPersonViewWithFrame:(CGRect)frame {
    if ([self.people count] == 0) {
        return nil;
    }
    
    // UIView+MDCSwipeToChoose and MDCSwipeToChooseView are heavily customizable.
    // Each take an "options" argument. Here, we specify the view controller as
    // a delegate, and provide a custom callback that moves the back card view
    // based on how far the user has panned the front card view.
    MDCSwipeToChooseViewOptions *options = [MDCSwipeToChooseViewOptions new];
    options.delegate = self;
    options.threshold = 160.f;
    options.likedColor = UIColorFromRGB(COLOR_LIKE);
    options.likedText = @"";
    options.nopeColor = UIColorFromRGB(COLOR_NOPE);
    options.nopeText = @"";
    
    
    options.onPan = ^(MDCPanState *state){
        CGRect frame = [self backCardViewFrame];
        self.backCardView.frame = CGRectMake(frame.origin.x,
                                             frame.origin.y - (state.thresholdRatio * 10.f),
                                             CGRectGetWidth(frame),
                                             CGRectGetHeight(frame));
    };
    
    // Create a personView with the top person in the people array, then pop
    // that person off the stack.
    ChoosePersonView *personView = [[ChoosePersonView alloc] initWithFrame:frame
                                                                    person:self.people[0]
                                                                   options:options];
    personView.delegate = self;
    [self.people removeObjectAtIndex:0];
    
    return personView;
}

#pragma mark - View Contruction

- (CGRect)frontCardViewFrame {
    
    CGFloat horizontalPadding = 20.f;
    CGFloat topPadding = 84.f;
    CGFloat bottomPadding = 230.f;
    
    return CGRectMake(horizontalPadding,
                      topPadding,
                      CGRectGetWidth(self.view.frame) - (horizontalPadding * 2),
                      CGRectGetHeight(self.view.frame) - bottomPadding);
    
}

- (CGRect)backCardViewFrame {
    CGRect frontFrame = [self frontCardViewFrame];
    
    return CGRectMake(frontFrame.origin.x,
                      frontFrame.origin.y + 10.f,
                      CGRectGetWidth(frontFrame),
                      CGRectGetHeight(frontFrame));
    
}

// Create and add the "nope" button.
- (void)constructNopeButton {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *image = [UIImage imageNamed:@"icon_multiply70"];
    button.frame = CGRectMake(ChoosePersonButtonHorizontalPadding,
                              CGRectGetMaxY(self.backCardView.frame) + ChoosePersonButtonVerticalPadding,
                              ChoosePersonButtonWidth - 5,
                              ChoosePersonButtonHeight - 5);
    [button setImage:image forState:UIControlStateNormal];
    
    [button setTintColor:UIColorFromRGB(COLOR_NOPE)];
    
    [button addTarget:self
               action:@selector(nopeFrontCardView)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
}

// Create and add the "like" button.
- (void)constructLikedButton {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *image = [UIImage imageNamed:@"icon_heart"];
    
//    button.frame = CGRectMake(CGRectGetMaxX(self.view.frame) - image.size.width - ChoosePersonButtonHorizontalPadding,
//                              CGRectGetMaxY(self.backCardView.frame) + ChoosePersonButtonVerticalPadding,
//                              image.size.width,
//                              image.size.height);
    
    button.frame = CGRectMake(CGRectGetMaxX(self.view.frame) - ChoosePersonButtonWidth - ChoosePersonButtonHorizontalPadding,
                              CGRectGetMaxY(self.backCardView.frame) + ChoosePersonButtonVerticalPadding,
                              ChoosePersonButtonWidth,
                              ChoosePersonButtonHeight);
    
    [button setImage:image forState:UIControlStateNormal];
    
    
    [button setTintColor:UIColorFromRGB(COLOR_LIKE)];
    
    [button addTarget:self
               action:@selector(likeFrontCardView)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
}

// Create and add the "info" button.
- (void)constructInfoButton {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *image = [UIImage imageNamed:@"icon_info"];
    
    button.frame = CGRectMake(CGRectGetMaxX(self.view.frame) / 2 - ChoosePersonButtonWidth / 2 + 5,
                              CGRectGetMaxY(self.backCardView.frame) + ChoosePersonButtonVerticalPadding + 5,
                              ChoosePersonButtonWidth - 15,
                              ChoosePersonButtonHeight - 15);
    
    [button setImage:image forState:UIControlStateNormal];
    
    
    [button setTintColor:UIColorFromRGB(COLOR_BLUE_UNIMATCH)];
    
    [button addTarget:self action:@selector(infoFrontCardView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
}

#pragma mark - Control Events

// Programmatically "nopes" the front card view.
- (void)nopeFrontCardView {
    [self.frontCardView mdc_swipe:MDCSwipeDirectionLeft];
}

// Programmatically "likes" the front card view.
- (void)likeFrontCardView {
    [self.frontCardView mdc_swipe:MDCSwipeDirectionRight];
}

- (void)infoFrontCardView{
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAUserPhotoViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:nil];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
