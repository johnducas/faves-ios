//
// ChoosePersonView.m
//
// Copyright (c) 2014 to present, Brian Gesiak @modocache
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "ChoosePersonView.h"
#import "ImageLabelView.h"
#import "Person.h"

#import <JCRBlurView.h>
#import <ILTranslucentView.h>

static const CGFloat ChoosePersonViewImageLabelWidth = 50.f;

@interface ChoosePersonView (){
    UITapGestureRecognizer *            tapGesture;
    
}
@property (nonatomic, strong) UIView *informationView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *uniLabel;
@property (nonatomic, strong) ImageLabelView *cameraImageLabelView;
@property (nonatomic, strong) ImageLabelView *interestsImageLabelView;
@property (nonatomic, strong) ImageLabelView *friendsImageLabelView;
@end

@implementation ChoosePersonView

#pragma mark - Object Lifecycle

- (instancetype)initWithFrame:(CGRect)frame
                       person:(Person *)person
                      options:(MDCSwipeToChooseViewOptions *)options {
    self = [super initWithFrame:frame options:options];
    if (self) {
        _person = person;
        
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight |
                                UIViewAutoresizingFlexibleWidth |
                                UIViewAutoresizingFlexibleBottomMargin;
        self.imageView.autoresizingMask = self.autoresizingMask;

        [self constructInformationView];
        
        self.imageView.image = _person.image;
        
        [self setClipsToBounds:YES];
        
        tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTap:)];
        tapGesture.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

#pragma mark - Tap Gesture
- (void)actionTap : (UITapGestureRecognizer *)gesture{
    
    NSLog(@"tap gesture = %@", self.person.name);
    
    if ([self.delegate respondsToSelector:@selector(actionChoosePersonViewTouched:)]) {
        [self.delegate actionChoosePersonViewTouched:self.person];
    }
    
}

#pragma mark - Internal Methods

- (void)constructInformationView {
    
    CGFloat bottomHeight = 70.f;
    CGRect bottomFrame = CGRectMake(0,
                                    CGRectGetHeight(self.bounds) - bottomHeight,
                                    CGRectGetWidth(self.bounds),
                                    bottomHeight);
    _informationView = [[UIView alloc] initWithFrame:bottomFrame];
    _informationView.backgroundColor = [UIColor clearColor];
    _informationView.clipsToBounds = YES;
    _informationView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
                                        UIViewAutoresizingFlexibleTopMargin;
    
    [self addSubview:_informationView];
    
    [self constructBackgroundImage];
    [self constructNameLabel];
    [self constructUniversityLabel];
//    [self constructCameraImageLabelView];
    [self constructInterestsImageLabelView];
    [self constructFriendsImageLabelView];
}

- (void)constructBackgroundImage{
    
    CGRect frame = _informationView.bounds;
    
    ILTranslucentView *translucentView = [[ILTranslucentView alloc] initWithFrame:frame];
     //that's it :)
    
    //optional:
    translucentView.translucentAlpha = 1;
    translucentView.translucentStyle = UIBarStyleDefault;
    translucentView.translucentTintColor = [UIColor clearColor];
    translucentView.backgroundColor = [UIColor clearColor];
    
    [_informationView addSubview:translucentView];
    
//    JCRBlurView *blurView = [JCRBlurView new];
//    blurView.tintColor = FlatWhite;
//    [blurView setFrame:frame];
//    [_informationView addSubview:blurView];
    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:frame];
    
//    imgView.backgroundColor = UIColorFromRGBWithAlpha(COLOR_BLUE_UNIMATCH, 0.8);
    
//    imgView.backgroundColor = [UIColor colorWithWhite:1 alpha:.6];
    
    
//    [_informationView addSubview:imgView];
    
}

- (void)constructNameLabel {
    CGFloat leftPadding = 15.f;
    CGFloat topPadding = 5.f;
    CGRect frame = CGRectMake(leftPadding,
                              topPadding,
                              CGRectGetWidth(_informationView.frame)  - leftPadding - 120,
                              CGRectGetHeight(_informationView.frame) / 2 - topPadding);
    
    _nameLabel = [[UILabel alloc] initWithFrame:frame];
    _nameLabel.text = [NSString stringWithFormat:@"%@, %@", _person.name, @(_person.age)];
    _nameLabel.textColor = FlatBlack;
    _nameLabel.font = [UIFont fontWithName:FONT_HELVETICA_NEUE size:16];
    
    [_informationView addSubview:_nameLabel];
}

- (void)constructUniversityLabel{
    
    CGFloat leftPadding = 15.f;
    CGFloat topPadding = 5.f;
    CGRect frame = CGRectMake(leftPadding,
                              CGRectGetHeight(_informationView.frame) / 2,
                              floorf(CGRectGetWidth(_informationView.frame) / 2),
                              CGRectGetHeight(_informationView.frame) / 2 - topPadding);
    
    _uniLabel = [[UILabel alloc] initWithFrame:frame];
    _uniLabel.text = [NSString stringWithFormat:@"%@", _person.university];
    _uniLabel.textColor = FlatGrayDark;
    _uniLabel.font = [UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:14];
    
    [_informationView addSubview:_uniLabel];
    
    
}

- (void)constructCameraImageLabelView {
    CGFloat rightPadding = 10.f;
    UIImage *image = [UIImage imageNamed:@"icon_heart"];
    _cameraImageLabelView = [self buildImageLabelViewLeftOf:CGRectGetWidth(_informationView.bounds) - rightPadding
                                                      image:image
                                                       text:[@(_person.numberOfPhotos) stringValue]];
    [_informationView addSubview:_cameraImageLabelView];
}

- (void)constructInterestsImageLabelView {
    CGFloat rightPadding = 15.f;
    UIImage *image = [UIImage imageNamed:@"liked"];
    _interestsImageLabelView = [self buildImageLabelViewLeftOf:CGRectGetWidth(_informationView.bounds) - rightPadding
                                                         image:image
                                                          text:[@(_person.numberOfSharedInterests) stringValue]];
    [_informationView addSubview:_interestsImageLabelView];
}

- (void)constructFriendsImageLabelView {
    UIImage *image = [UIImage imageNamed:@"group"];
    _friendsImageLabelView = [self buildImageLabelViewLeftOf:CGRectGetMinX(_interestsImageLabelView.frame)
                                                      image:image
                                                       text:[@(_person.numberOfSharedFriends) stringValue]];
    [_informationView addSubview:_friendsImageLabelView];
}

- (ImageLabelView *)buildImageLabelViewLeftOf:(CGFloat)x image:(UIImage *)image text:(NSString *)text {
    CGRect frame = CGRectMake(x - ChoosePersonViewImageLabelWidth,
                              0,
                              ChoosePersonViewImageLabelWidth,
                              CGRectGetHeight(_informationView.bounds));
    ImageLabelView *view = [[ImageLabelView alloc] initWithFrame:frame
                                                           image:image
                                                            text:text];
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    return view;
}

@end
