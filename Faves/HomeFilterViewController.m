//
//  HomeFilterViewController.m
//  Faves
//
//  Created by Miloslav Sorokin on 23/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "HomeFilterViewController.h"

@interface HomeFilterViewController ()

@end

@implementation HomeFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"Settings"];
    
    UIBarButtonItem *btnClose = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(actionClose:)];
//    [self.navigationItem setLeftBarButtonItem:btnClose];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(actionDone:)];
    [self.navigationItem setRightBarButtonItem:btnDone];
    
    
}

#pragma mark - Actions
- (void)actionClose : (id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionDone : (id)sender{
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - UITableView Delegate and Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            return 1;
        }
        case 1:{
            return 1;
        }
        case 2:{
            return 3;
        }
        case 3:{
            return 1;
        }
            
        default:
            return 0;
            break;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"cellIdentifier";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSString *strText;
    
    switch (indexPath.section) {
        case 0:{
            
            strText = @"Filter";
            break;
            
        }
        case 1:{
            strText = @"Basic Information";
            break;
        }
        case 2:{
            switch (indexPath.row) {
                case 0:{
                    strText = @"About";
                    break;
                }
                case 1:{
                    strText = @"Help Center";
                    break;
                }
                case 2:{
                    strText = @"Privacy";
                    break;
                }
                default:
                    break;
            }
            
            break;
        }
        case 3:{
            strText = @"Log Out";
            break;
        }
        default:
            break;
    }
    
    [cell.textLabel setText:strText];
    
    [cell.textLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    [cell.textLabel setTextColor:FlatBlack];
    
    return cell;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *strSegue;
    
    switch (indexPath.section) {
        case 0:{
            
            strSegue = @"FAFilterViewController";
            
            break;
        }
        case 1:{
            
            strSegue = @"FABasicInfoViewController";
            
            break;
        }
        case 2:{
            switch (indexPath.row) {
                case 0:{
                    
                    strSegue = @"FAAboutViewController";
                    break;
                }
                case 1:{
                    
                    strSegue = @"FAHelpCenterViewController";
                    break;
                }
                case 2:{
                    
                    strSegue = @"FAPrivacyViewController";
                    break;
                }
                default:
                    break;
            }
            
            break;
        }
        case 3:{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure want to log out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Log Out", nil];
            [alert show];
            
            break;
        }
        default:
            break;
    }
    
    if (strSegue) {
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:strSegue];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
