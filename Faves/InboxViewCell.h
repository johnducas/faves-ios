//
//  InboxViewCell.h
//  Faves
//
//  Created by Miloslav Sorokin on 23/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxViewCell : UITableViewCell{
    
    IBOutlet UIImageView *              imgProfile;
    
    IBOutlet UILabel *                  lblName;
    
    IBOutlet UILabel *                  lblMessage;
    
}

@end
