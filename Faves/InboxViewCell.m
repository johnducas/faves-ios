//
//  InboxViewCell.m
//  Faves
//
//  Created by Miloslav Sorokin on 23/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "InboxViewCell.h"

@implementation InboxViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [imgProfile.layer setCornerRadius:25];
    [imgProfile.layer setMasksToBounds:YES];
    
    [lblName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    [lblName setTextColor:FlatBlack];
    
    [lblMessage setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:14]];
    [lblMessage setTextColor:FlatBlackDark];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
