//
//  FAUserProfileHashtagViewCell.h
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKTagView;
@interface FAUserProfileHashtagViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SKTagView *tagView;

+ (instancetype)instantiateFromNib;

- (void)configureCell : (NSArray *)array;
@end
