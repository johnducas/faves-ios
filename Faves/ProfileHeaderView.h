//
//  ProfileHeaderView.h
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileHeaderView : UIView{
    
//    IBOutlet UIImageView *                  imgBack;
//    
//    IBOutlet UIImageView *                  imgProfile;
//    
//    IBOutlet UILabel *                      lblName;
}


@property (nonatomic, assign) IBOutlet UIImageView *                  imgBack;
@property (nonatomic, assign) IBOutlet UIImageView *                  imgProfile;
@property (nonatomic, assign) IBOutlet UILabel *                      lblName;


- (void)setImageProfile :(UIImage *)_image;

@end
