//
//  SearchViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "SearchViewController.h"
#import "CategorySearchViewController.h"

@interface SearchViewController (){
//    NSArray *                   arrayCategories;
    
    UIBarButtonItem *           btnMenu;
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [tblList deselectRowAtIndexPath:tblList.indexPathForSelectedRow animated:NO];
}

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.view setBackgroundColor:[UIColor flatWhiteColor]];
    
    searchContent = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [searchContent setDelegate:self];
    searchContent.searchBarStyle = UISearchBarStyleMinimal;
    searchContent.placeholder = @"Search";
    [searchContent setTintColor:FlatWhite];
    searchContent.showsScopeBar = YES;
    
    [self.navigationItem setTitleView:searchContent];
    
    [tblList setBackgroundColor:[UIColor clearColor]];
    
}

#pragma mark - Actions



#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [AppManager sharedManager].arrayCategory.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *   strIdentifier = @"categoryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    FACategory *category = [AppManager sharedManager].arrayCategory[indexPath.row];
    
    [cell.textLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:16]];
    [cell.textLabel setText:category.name];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CategorySearchViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategorySearchViewController"];
//    viewController.strCategory = [[AppManager sharedManager].arrayCategoryName objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:viewController animated:YES];

    
}

#pragma mark - Search bar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;                      // return NO to not become first responder
{
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;                     // called when text starts editing
{
    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar;                        // return NO to not resign first responder
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [self.navigationItem setLeftBarButtonItem:btnMenu animated:YES];
    return YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;                       // called when text ends editing
{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;   // called when text changes (including clear)
{
    
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0); // called before text changes
{
    
    return YES;
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;                     // called when keyboard search button pressed
{
//    [searchBar setShowsCancelButton:YES animated:YES];
    [searchBar resignFirstResponder];
    
}
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar;                   // called when bookmark button pressed
{
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar;                     // called when cancel button pressed
{
    [searchBar resignFirstResponder];
}
- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar NS_AVAILABLE_IOS(3_2); // called when search results button pressed
{
    
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope NS_AVAILABLE_IOS(3_0);
{
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@""]) {
    }
}


@end
