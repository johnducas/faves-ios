//
//  FAOtherViewController.m
//  Faves
//
//  Created by Chen Wang on 4/13/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAOtherViewController.h"
#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>

#import "ProfileHeaderView.h"

@interface FAOtherViewController (){
    ProfileHeaderView *     headerView;
}

@end

@implementation FAOtherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    [self loadProfile];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    self.navigationItem.title    = @"Other";
    
    UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(actionEdit:)];
    self.navigationItem.rightBarButtonItem = btnEdit;
    
    headerView = [[ProfileHeaderView alloc] init];
    
    [self.tableView setParallaxHeaderView:headerView
                                     mode:VGParallaxHeaderModeFill // For more modes have a look in UIScrollView+VGParallaxHeader.h
                                   height:self.view.frame.size.width * 3/5];
    
    [self.view setBackgroundColor:FlatWhite];
    
    [btnSettings.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:20]];
    
    [btnSupport.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:20]];
    
    [btnHelp.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:20]];
    
    [btnSettings setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    
    [btnSupport setTitleEdgeInsets:UIEdgeInsetsMake(0, 30, 0, 0)];
    
    
    [btnHelp.layer setCornerRadius:4.f];
    [btnHelp.layer setBorderWidth:1.f];
    [btnHelp.layer setBorderColor:UIColorFromRGB(COLOR_BLUE_UNIMATCH).CGColor];
    
}

- (void)loadProfile{
    
    headerView.lblName.text     = @"John Ducas";
    
    [headerView setImageProfile:[UIImage imageNamed:@"john1.jpeg"]];
    
}

#pragma mark - Actions
- (void)actionEdit : (id)sender{
    
}

- (IBAction)actionSetting:(id)sender;{
    
}

- (IBAction)actionSupport:(id)sender;{
    
}

- (IBAction)actionHelp:(id)sender;{
    
    NSString *textToShare = @"Have you heard of UniMatch? It's a network ONLY for students";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.faves.com"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // This must be called in order to work
    [self.tableView shouldPositionParallaxHeader];
    
    // scrollView.parallaxHeader.progress - is progress of current scroll
    NSLog(@"Progress: %f", scrollView.parallaxHeader.progress);
    
}


//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
