//
//  AppManager.h
//  Kwibal
//
//  Created by Chen Wang on 5/2/14.
//  Copyright (c) 2014 Kwibal. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FACategory.h"
#import "FALike.h"

@interface AppManager : NSObject

@property (nonatomic, strong) NSArray *             arrayCategoryID;
@property (nonatomic, strong) NSArray *             arrayCategoryName;

@property (nonatomic, strong) NSMutableArray *      arrayCategory;
@property (nonatomic, strong) NSMutableArray *      arrayLikes;

+ (AppManager* ) sharedManager;

#pragma mark - Category
- (void)saveCategory;
- (void)loadCategory;

- (BOOL)isCategoryExist : (FACategory *)category;
- (BOOL)isLikePageExist : (FALike *)category;



#pragma mark - Image URL from Facebook Id
- (NSString *)imageUrlFromFbId : (NSString *) _fbId;

#pragma mark - Show Alert
- (void)showAlert : (NSString *)title message : (NSString *)message;

@end


#pragma mark - Static Strings
extern NSString * const FETCHLIKES_MINE_COMPLETED;

