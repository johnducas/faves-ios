//
//  FAProfilePhotoView.h
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAProfilePhotoView : UIView{
    
    IBOutlet UIImageView *          imgPhoto;
}

@property (nonatomic, weak) NSString *      imgUrl;

+ (instancetype)instantiateFromNib;

@end
