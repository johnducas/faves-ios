//
//  FBPage.m
//  Faves
//
//  Created by Miloslav Sorokin on 22/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "FBPage.h"

@implementation FBPage


#pragma mark - Shared Functions
+ ( id ) me
{
    __strong static FBPage*     sharedObject = nil ;
    static dispatch_once_t onceToken ;
    
    dispatch_once( &onceToken, ^{
        sharedObject = [ [ FBPage alloc ] init ] ;
    } ) ;
    
    return sharedObject ;
}

#pragma mark - Init with Dictionary -
- ( id ) initWithDict : ( NSDictionary* ) _dict
{
    self = [ super init ] ;
    
    if( self )
    {
        
    }
    
    return self ;
    
}

- ( id ) initWithFB : ( NSDictionary* ) _dict ;{
    self = [ super init ] ;
    
    if( self )
    {
        
        self.fbId               = [_dict objectForKey:@"id"];
        self.categoryName       = [_dict objectForKey:@"category"];
        self.name               = [_dict objectForKey:@"name"];

//        NSUInteger index = [[AppManager sharedManager].arrayCategoryName indexOfObject:[_dict objectForKey:@"category"]];
//        
//        if (index != NSNotFound) {
//            self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:index];
//        }
//        
        
        
        for (int i = 0 ; i < [AppManager sharedManager].arrayCategoryName.count ; i++) {
            
//            if (NSOrderedSame == [[_dict objectForKey:@"category"] localizedCompare:[AppManager sharedManager].arrayCategoryName[i]]){
//                self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:i];
//                break;
//            }
            
            NSLog(@"compared result = %d, %@, %@", [self.categoryName compare:[AppManager sharedManager].arrayCategoryName[i]], self.categoryName, [AppManager sharedManager].arrayCategoryName[i]);
            
            if ([self.categoryName caseInsensitiveCompare:[AppManager sharedManager].arrayCategoryName[i]] == NSOrderedSame) {
                self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:i];
                break;
            }
            
            
            
//            if ([self.categoryName compare:[AppManager sharedManager].arrayCategoryName[i]]) {
//                self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:i];
//                break;
//                
//            }
            
//            if ([[_dict objectForKey:@"category"] isEqualToString:[[AppManager sharedManager].arrayCategoryName objectAtIndex:i]]) {
//                self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:i];
//                break;
//            }
            
        }
        
        NSLog(@"category name = %@", self.categoryName);
        
//        if([[AppManager sharedManager].arrayCategoryName containsObject:[_dict objectForKey:@"category"]]){
//            NSLog(@"found");
//        }
        
//        int index = [self findName:self.categoryName];
//        
//        if (index >= 0) {
//            self.categoryId = [[AppManager sharedManager].arrayCategoryID objectAtIndex:index];
//        }
        
        
    }
    
    return self ;
    
}

-(int)findName:(NSString *)name {
    int min, mid, max;
    NSComparisonResult comparisonResult;
    min = 0;
    max = [[AppManager sharedManager].arrayCategoryName count]-1;
    while (min <= max) {
        mid = min + (max-min)/2;
        comparisonResult = [name compare:[[AppManager sharedManager].arrayCategoryName objectAtIndex:mid]];
        if (comparisonResult == NSOrderedSame) {
            return mid;
        } else if (comparisonResult == NSOrderedDescending) {
            min = mid+1;
        } else {
            max = mid-1;
        }
    }
    return -1;
}


@end
