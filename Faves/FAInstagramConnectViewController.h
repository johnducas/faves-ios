//
//  FAInstagramConnectViewController.h
//  Faves
//
//  Created by Chen Wang on 04/03/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAInstagramConnectViewController : UITableViewController{
    
    IBOutlet UILabel *          lblTitle;
    
    IBOutlet UIButton *         btnInstagram;
    
    
}


- (IBAction)actionInstagram:(id)sender;

@end
