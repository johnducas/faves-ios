//
//  SearchPageViewCell.m
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "SearchPageViewCell.h"

@implementation SearchPageViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.lblName setTextColor:FlatWhite];
    [self.lblName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:16]];
    
}

@end
