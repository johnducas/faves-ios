//
//  FAUserProfileHashtagViewCell.m
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAUserProfileHashtagViewCell.h"
#import <SKTagView.h>
#import <SKTag.h>
#import <SKTagButton.h>

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)



@implementation FAUserProfileHashtagViewCell

+ (instancetype)instantiateFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"%@", [self class]] owner:nil options:nil];
    return [views firstObject];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(NSArray *)array{
    
    self.tagView.preferredMaxLayoutWidth = SCREEN_WIDTH;
    self.tagView.padding    = UIEdgeInsetsMake(10, 10, 10, 10);
    self.tagView.insets    = 15;
    self.tagView.lineSpace = 10;
    
    [self.tagView removeAllTags];
    
    //Add Tags
    [@[@"Python", @"Javascript", @"HTML", @"Go", @"Objective-C",@"C", @"PHP"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
     {
         SKTag *tag = [SKTag tagWithText:obj];
         tag.textColor = [UIColor whiteColor];
         tag.fontSize = 12;
         tag.padding = UIEdgeInsetsMake(5, 5, 5, 5);
         tag.bgColor = RandomFlatColor;
         tag.cornerRadius = 5;
         
         [self.tagView addTag:tag];
     }];
    
}

@end
