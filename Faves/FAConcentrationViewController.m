//
//  FAConcentrationViewController.m
//  Faves
//
//  Created by Chen Wang on 04/03/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAConcentrationViewController.h"

@interface FAConcentrationViewController ()<UIAlertViewDelegate>{
    
    NSArray *       arrayHumanities;
    NSArray *       arraySocial;
    NSArray *       arrayNatural;
    NSArray *       arrayFormal;
}

@end

@implementation FAConcentrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadList];
    
    self.navigationItem.title = @"Concentration";
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(actionNext:)];
    [self.navigationItem setRightBarButtonItem:btnDone];
    
    self.tableView.tableHeaderView.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load List
- (void)loadList{
    
    arrayHumanities = @[@"Human history",
                        @"Linguistics",
                        @"Literature",
                        @"Arts",
                        @"Philosophy",
                        @"Religion"];
    
    arraySocial = @[@"Anthropology",
                    @"Archaeology",
                    @"Area studies",
                    @"Cultural and ethnic studies",
                    @"Economics",
                    @"Gender and sexuality studies",
                    @"Geography",
                    @"Political science",
                    @"Psychology",
                    @"Sociology"];
    
    arrayNatural = @[@"Biology",
                     @"Chemistry",
                     @"Earth sciences",
                     @"Physics",
                     @"Space sciences"];
    
    arrayFormal = @[@"Applied Mathematics", @"Pure Mathematics",
                    @"Compute sciences",
                    @"Logic",
                    @"Statistics",
                    @"Systems science"];
    
//    [self.tableView reloadData];
    
}

#pragma mark - Actions
- (void)actionNext : (id)sender{
    
    if ([self.tableView indexPathForSelectedRow]) {
        
        UIAlertView *alertEmail = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter school email to confirm" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        alertEmail.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertEmail.tag = 100;
        [alertEmail show];
        
    }else{
        
        [[AppManager sharedManager] showAlert:@"Error!" message:@"Please select your concentration"];
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    switch (section) {
        case 0:{
            return arrayHumanities.count;
            break;
        }
        case 1:{
            return arraySocial.count;
            break;
        }
        case 2:{
            return arrayNatural.count;
            break;
        }
        case 3:{
            return arrayFormal.count;
            break;
        }
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *strIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier];
    }
    
    
    NSMutableString *strText = [NSMutableString string];
    
    [strText appendString:@" "];
    
    switch (indexPath.section) {
        case 0:{
            
//            cell.textLabel.text = arrayHumanities[indexPath.row];
            [strText appendString:arrayHumanities[indexPath.row]];
            
            break;
        }
        case 1:{
//            cell.textLabel.text = arraySocial[indexPath.row];
            [strText appendString:arraySocial[indexPath.row]];
            break;
        }
        case 2:{
//            cell.textLabel.text = arrayNatural[indexPath.row];
            [strText appendString:arrayNatural[indexPath.row]];
            break;
        }
        case 3:{
//            cell.textLabel.text = arrayFormal[indexPath.row];
            [strText appendString:arrayFormal[indexPath.row]];
            break;
        }
            
        default:
            break;
    }
    
    if (indexPath == [tableView indexPathForSelectedRow]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = strText;
    cell.textLabel.font = [UIFont fontWithName:FONT_HELVETICA_NEUE size:14];
    
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;{
    switch (section) {
        case 0:{
            return @"Humanities";
            break;
        }
        case 1:{
            return @"Social Sciences";
            break;
        }
        case 2:{
            return @"Natural Sciences";
            break;
        }
        case 3:{
            return @"Formal Sciences";
            break;
        }
            
        default:
            break;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
}

#pragma mark - UIAlertView Delegate
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation{
{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            
            UITextField *txtEmail = [alertView textFieldAtIndex:0];
            
            if ([NSString NSStringIsValidEmail:txtEmail.text]) {
                UIAlertView *alertEmail = [[UIAlertView alloc] initWithTitle:@"Great!" message:@"We will send you a unique code to enter on the app. You can still get matches but they will be notified until you've confirmed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                alertEmail.tag = 200;
                [alertEmail show];
            }else{
                
                [[AppManager sharedManager] showAlert:@"Error!" message:@"Email address is invalid"];
                
            }
//            [[AppManager sharedManager] showAlert:@"Great!" message:@"We will send you a unique code to enter on the app. You can still get matches will be notified until you've confirmed"];
            
            
            
        }
        
    }else if(alertView.tag == 200){
        
        UIViewController *viewController = [UIStoryboard initiateStorybard:STORYBOARD_MAIN withViewController:@"MainTabViewController"];
        viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:viewController animated:YES completion:nil];
        
        
    }
    
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
