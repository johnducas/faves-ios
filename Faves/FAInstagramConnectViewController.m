//
//  FAInstagramConnectViewController.m
//  Faves
//
//  Created by Chen Wang on 04/03/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAInstagramConnectViewController.h"

#import <InstagramKit.h>


@interface FAInstagramConnectViewController ()<UIWebViewDelegate>{
    
    UIWebView *     mWebView;
    
}

@end

@implementation FAInstagramConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    [self updateFonts];
}

#pragma mark - loadController
- (void)loadController{
    
    self.navigationItem.title = @"Instagram";
    
    [self.navigationItem setHidesBackButton:YES];
    
    
    UIBarButtonItem *btnNext = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(actionNext:)];
    [self.navigationItem setRightBarButtonItem:btnNext];
    
    [self.tableView setScrollEnabled:NO];
    
    mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    mWebView.scrollView.bounces = NO;
    mWebView.contentMode = UIViewContentModeScaleAspectFit;
    mWebView.delegate = self;
    
}

- (void)updateFonts{
    
    [lblTitle setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    
}

#pragma mark - Actions

- (void)actionNext:(id)sender{
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAUniversityViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (IBAction)actionInstagram:(id)sender{
    
    NSDictionary *configuration = [InstagramEngine sharedEngineConfiguration];
//    NSString *scopeString = [InstagramEngine stringForScope:self.scope];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?client_id=%@&redirect_uri=%@&response_type=token", configuration[kInstagramKitAuthorizationUrlConfigurationKey], configuration[kInstagramKitAppClientIdConfigurationKey], configuration[kInstagramKitAppRedirectUrlConfigurationKey]]];
    
    [mWebView loadRequest:[NSURLRequest requestWithURL:url]];
    
    
    [[InstagramEngine sharedEngine] loginWithBlock:^(NSError *error) {
        
        NSLog(@"error =%@", error);
        
    }];
    
}

//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    NSString *URLString = [request.URL absoluteString];
//    if ([URLString hasPrefix:[[InstagramEngine sharedEngine] appRedirectURL]]) {
//        NSString *delimiter = @"access_token=";
//        NSArray *components = [URLString componentsSeparatedByString:delimiter];
//        if (components.count > 1) {
//            NSString *accessToken = [components lastObject];
//            NSLog(@"ACCESS TOKEN = %@",accessToken);
//            [[InstagramEngine sharedEngine] setAccessToken:accessToken];
//            
//            [self dismissViewControllerAnimated:YES completion:^{
//                [self.collectionViewController reloadMedia];
//            }];
//        }
//        return NO;
//    }
//    return YES;
//}
//
//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
//{
//    

    



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
