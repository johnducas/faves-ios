//
//  FBPage.h
//  Faves
//
//  Created by Miloslav Sorokin on 22/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBPage : NSObject

@property (nonatomic, strong) NSString *            pageId;
@property (nonatomic, strong) NSString *            fbId;
@property (nonatomic, strong) NSString *            categoryName;
@property (nonatomic, strong) NSString *            categoryId;
@property (nonatomic, strong) NSString *            name;
@property (nonatomic, strong) NSString *            imageUrl;

+ ( id ) me;
- ( id ) initWithFB : ( NSDictionary* ) _dict ;
- ( id ) initWithDict : ( NSDictionary* ) _dict ;

@end
