//
//  SearchPageViewCell.h
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPageViewCell : UICollectionViewCell{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *         imgProfile;
@property (nonatomic, strong) IBOutlet UILabel *             lblName;

@end
