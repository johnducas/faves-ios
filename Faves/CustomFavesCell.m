//
//  CustomFavesCell.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "CustomFavesCell.h"

@implementation CustomFavesCell

- (void)awakeFromNib {
    // Initialization code
    
    [viewContent.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewContent.layer setShadowOffset:CGSizeMake(2, 2)];
    [viewContent.layer setShadowOpacity:.3];
    [viewContent.layer setShadowRadius:1];
    
    [viewBottom.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewBottom.layer setShadowOffset:CGSizeMake(1, 1)];
    [viewBottom.layer setShadowOpacity:.3];
    [viewBottom.layer setShadowRadius:1];
    
    [btnAvatar.layer setCornerRadius:btnAvatar.frame.size.width/2];
    [btnAvatar.layer setMasksToBounds:YES];
    [btnAvatar.layer setBorderColor:[UIColor blackColor].CGColor];
    [btnAvatar.layer setBorderWidth:1.0f];
    
    [lblTitle setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_BOLD size:20]];
    
    [lblDescription setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:14]];
    
    [lblUserName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
