//
//  FBCategory.m
//  Faves
//
//  Created by Miloslav Sorokin on 22/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "FBCategory.h"

@implementation FBCategory
#pragma mark - Shared Functions
+ ( id ) me
{
    __strong static FBCategory*     sharedObject = nil ;
    static dispatch_once_t onceToken ;
    
    dispatch_once( &onceToken, ^{
        sharedObject = [ [ FBCategory alloc ] init ] ;
    } ) ;
    
    return sharedObject ;
}

#pragma mark - Init with Dictionary -
- ( id ) initWithDict : ( NSDictionary* ) _dict
{
    self = [ super init ] ;
    
    if( self )
    {
        
        
    }
    
    return self ;
    
}


@end
