//
//  CustomFavesCell.h
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFavesCell : UITableViewCell{
    
    IBOutlet UIView *                   viewContent;
    
    IBOutlet UIImageView *              imgContent;
    
    IBOutlet UIView *                   viewDesc;
    
    IBOutlet UIImageView *              imgDescBack;
    
    IBOutlet UILabel *                  lblTitle;
    IBOutlet UILabel *                  lblDescription;
    
    
    IBOutlet UIView *                   viewBottom;
    IBOutlet UIButton *                 btnAvatar;
    IBOutlet UILabel *                  lblUserName;
    IBOutlet UIButton *                 btnShare;
    
}

@end
