//
//  MainNavController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "MainNavController.h"

@interface MainNavController ()

@end

@implementation MainNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
//    [[UINavigationBar appearance] setBarTintColor:FlatBlue];
//    [[UINavigationBar appearance] setBarTintColor:FlatBlueDark];
//    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(COLOR_BLUE_MAIN)];
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(COLOR_BLUE_UNIMATCH)];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
    [titleBarAttributes setValue:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:24] forKey:NSFontAttributeName];
    [titleBarAttributes setValue:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
    

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
