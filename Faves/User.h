//
//  User.h
//  OneNeed
//
//  Created by Chen Wang on 11/11/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : PFUser

@property (nonatomic, strong) NSString *                name;
@property (nonatomic, strong) NSString *                fbId;
@property (nonatomic, strong) NSString *                gender;
@property (nonatomic, strong) NSDate *                  birthday;


@end
