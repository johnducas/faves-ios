//
//  WelcomeViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController (){

    NSArray *               arrayPermission;
    
    
    BOOL                    bLikeCompleted;
    BOOL                    bCategoryCompleted;
    
}



@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    [self actionRegister];

//    return;
    
    
//    [[CommunicationManager sharedManager] fetchCategory];
    
    [self loadController];
    
    if ([User currentUser]) {
//        [[CommunicationManager sharedManager] fetchMine];

    }
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionLogin) name:FETCHLIKES_MINE_COMPLETED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionRegister) name:FETCHLIKES_MINE_COMPLETED object:nil];
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"Welcome"];

    
    [btnFacebook.titleLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:20]];
    [btnFacebook setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
//    arrayPermission =@[@"public_profile", @"email", @"user_friends"];
    arrayPermission =@[@"public_profile", @"email", @"user_friends", @"user_likes", @"user_birthday"];
    
}

#pragma mark - load Cateogry
- (void)loadCategory{
    
    PFQuery *query = [PFQuery queryWithClassName:[FACategory parseClassName]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            // Do something with the found objects
            for (PFObject *object in objects) {
                NSLog(@"%@", object.objectId);
                
                [object pinInBackground];
                
            }
            
        } else {
            
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            
            
        }
    }];
    
    
}


#pragma mark - Actions

- (IBAction)actionFacebook:(id)sender;{
    
    [self actionLogin];
//    [self actionRegister];
    
    return;
    
    if ([[PFFacebookUtils session] isOpen]) {
        [[PFFacebookUtils session] closeAndClearTokenInformation];
        
    }
    
    if ([User currentUser]) {
        [User logOut];
    }
    
    [SVProgressHUD show];
    
    [PFFacebookUtils logInWithPermissions:arrayPermission block:^(PFUser *user, NSError *error) {
        
        [SVProgressHUD dismiss];
        
        if (!user) {
            
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
            
            [[AppManager sharedManager] showAlert:@"Error!" message:error.localizedDescription];
            
        } else {
            
            FBRequestConnection *connection = [[FBRequestConnection alloc] init];
            
            if (user.isNew) {
                
                NSLog(@"User signed up and logged in through Facebook!");
                
                FBRequest *requestForMe = [FBRequest requestForMe];
                FBRequest *requestForFriends = [FBRequest requestForGraphPath:@"/me/friends?limit=10000"];

                [connection addRequest:requestForMe completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    if (error) {
                        NSLog(@"error = %@", error);
                    }else if (result){
                        NSLog(@"result = %@", result);
                        
                        [User currentUser].name         = [result objectForKey:@"name"];
                        [User currentUser].gender       = [result objectForKey:@"gender"];
                        [User currentUser].fbId         = [result objectForKey:@"id"];
                        [User currentUser].email        = [result objectForKey:@"email"];
                        
                        NSString *strBirthday = [result objectForKey:@"birthday"];
                        
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"MM/dd/yyyy"];
                        [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                        
                        [User currentUser].birthday     = [dateFormat dateFromString:strBirthday];
                        
                        [[User currentUser] saveEventually];
                        
                    }else{
                        NSLog(@"connection = %@", connection);
                    }
                }];
                
                [connection addRequest:requestForFriends completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    if (error) {
                        NSLog(@"error = %@", error);
                    }else if (result){
                        NSLog(@"result = %@", result);
                    }else{
                        NSLog(@"connection = %@", connection);
                    }
                }];
                
                FBRequest *requestForLikes = [FBRequest requestForGraphPath:@"/me/likes?limit=10000"];
                
                [connection addRequest:requestForLikes completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    
                    if (error) {
                        NSLog(@"error = %@", error);
                    }else if (result){
                        NSLog(@"result = %@", result);
                        
                        for (NSDictionary *dict in [result objectForKey:@"data"]) {
                            
                            FALike *like = [FALike object];
                            
                            like.id_user          = [User currentUser].objectId;
                            
                            like.id_like          = [dict objectForKey:@"id"];
                            like.category         = [dict objectForKey:@"category"];
                            like.name             = [dict objectForKey:@"name"];
                            
                            if (![[AppManager sharedManager] isLikePageExist:like]) {
                                [[AppManager sharedManager].arrayLikes addObject:like];
                            }
                            
                            if ([dict objectForKey:@"category_list"]) {
                                for (NSDictionary *_dict in [dict objectForKey:@"category_list"]) {
                                    
                                    FACategory *category = [FACategory object];
                                    
                                    category.id_category      = [_dict objectForKey:@"id"];
                                    category.name             = [_dict objectForKey:@"name"];
                                    
                                    if (![[AppManager sharedManager] isCategoryExist:category]) {
                                        [[AppManager sharedManager].arrayCategory addObject:category];
                                    }
                                    
                                }
                            }
                        }
                        
                        [PFObject saveAllInBackground:[AppManager sharedManager].arrayLikes block:^(BOOL succeeded, NSError *error) {
                            if (succeeded) {
                                for (FALike *like in [AppManager sharedManager].arrayLikes) {
                                    [like fetchIfNeededInBackground];
                                }
                                
//                                [self actionLogin];
                                [self actionRegister];
                                
                            }
                        }];
                        
                        [PFObject saveAllInBackground:[AppManager sharedManager].arrayCategory block:^(BOOL succeeded, NSError *error) {
                            
                            if (succeeded) {
                                for (FACategory *category in [AppManager sharedManager].arrayCategory) {
                                    [category fetchIfNeededInBackground];
                                }
                            }
                            
                        }];
                        
                    }else{
                        
                        NSLog(@"connection = %@", connection);
                        
                    }
                }];
                
            }else{
                
                NSLog(@"User logged in through Facebook!");
                
                [[CommunicationManager sharedManager] fetchMine];
                
            }
            
            [connection start];
            
        }
    }];
    
}

#pragma mark - login
- (void)actionLogin{
    
    
    UIViewController *viewController = [UIStoryboard initiateStorybard:STORYBOARD_MAIN withViewController:@"MainTabViewController"];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:viewController animated:YES completion:nil];
    
}

#pragma mark - Register
- (void)actionRegister{
    
//    UIViewController *viewController = [UIStoryboard initiateStorybard:STORYBOARD_MAIN withViewController:@"MainTabViewController"];
//    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:viewController animated:YES completion:nil];
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAInstagramConnectViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
