//
//  FAHelpCenterViewController.m
//  Faves
//
//  Created by Chen Wang on 19/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAHelpCenterViewController.h"

@interface FAHelpCenterViewController ()

@end

@implementation FAHelpCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"Help Center"];
    
    [self.view setBackgroundColor:FlatWhite];
    
}

#pragma mark - Actions


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
