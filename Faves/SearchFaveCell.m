//
//  SearchFaveCell.m
//  Faves
//
//  Created by Toki Kagawa on 11/19/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "SearchFaveCell.h"

@implementation SearchFaveCell

- (void)awakeFromNib {
    // Initialization code
    
    [viewContent.layer setShadowColor:[UIColor blackColor].CGColor];
    [viewContent.layer setShadowOffset:CGSizeMake(1, 1)];
    [viewContent.layer setShadowOpacity:.2];
    [viewContent.layer setShadowRadius:1];
    
    
    [lblTitle setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:18]];
    
    [lblDesc setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:14]];
    
    [btnAvatar.layer setCornerRadius:btnAvatar.frame.size.height/2];
    [btnAvatar.layer setMasksToBounds:YES];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionAvatar:(id)sender;{
    
}
- (IBAction)actionBookmark:(id)sender;{
    
    [btnBookmark setSelected:!btnBookmark.selected];
    
}

@end
