//
//  MessageViewController.h
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "JSQMessagesViewController.h"

#import "DemoModelData.h"
#import "NSUserDefaults+DemoSettings.h"


@interface MessageViewController : JSQMessagesViewController

@property (strong, nonatomic) DemoModelData *               demoData;

@end
