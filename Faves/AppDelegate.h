//
//  AppDelegate.h
//  Faves
//
//  Created by Toki Kagawa on 11/14/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

@end

