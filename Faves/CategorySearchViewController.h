//
//  CategorySearchViewController.h
//  Faves
//
//  Created by Toki Kagawa on 11/19/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorySearchViewController : UICollectionViewController<UICollectionViewDelegateFlowLayout>{
    
}

@property (nonatomic, strong) NSString *            strCategory;

@end
