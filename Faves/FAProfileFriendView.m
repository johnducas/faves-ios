//
//  FAProfileFriendView.m
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAProfileFriendView.h"

@implementation FAProfileFriendView

+ (instancetype)instantiateFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"%@", [self class]] owner:nil options:nil];
    return [views firstObject];
}

- (void)awakeFromNib{
    
    self.imgProfile.layer.cornerRadius = 25;
    self.imgProfile.clipsToBounds = YES;
    
    self.lblName.textColor = FlatBlack;
    self.lblName.font = [UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:12];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
