//
//  ProfilePageViewCell.m
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "ProfilePageViewCell.h"

@implementation ProfilePageViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [lblName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:20]];
    [lblName setTextColor:FlatBlack];
    
    [lblCategory setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    [lblCategory setTextColor:FlatBlackDark];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
