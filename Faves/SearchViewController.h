//
//  SearchViewController.h
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>{
    
    UISearchBar *                       searchContent;
    
    IBOutlet UITableView *              tblList;
    
}

@end
