//
//  ProfilePageViewCell.h
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePageViewCell : UITableViewCell{
    
    IBOutlet UILabel *              lblName;
    IBOutlet UILabel *              lblCategory;
    
}

@end
