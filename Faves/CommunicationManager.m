//
//  CommunicationManager.m
//  Pharmakon
//
//  Created by Yu Li on 4/30/14.
//  Copyright (c) 2014 Pharmakon. All rights reserved.
//

#import "CommunicationManager.h"
#import "User.h"

#define WEBSERVICE                              @"mobileservice.php"

#define LOGIN                                   @"login"
#define REGISTER                                @"register"

#define GET_CATEGORY                            @"get_category"

#define POST_PAGE                               @"post_page"



@interface CommunicationManager ()

- ( void ) sendToGetRequest : ( NSString * ) _service
                     params : ( NSDictionary * ) _params
                    success : ( void (^)( id _responseObject ) ) _success
                    failure : ( void (^)( NSError* _error ) ) _failure;

- ( void ) sendToPostRequest : ( NSString * ) _service
                      params : ( NSDictionary * ) _params
                     success : ( void (^)( id _responseObject ) ) _success
                     failure : ( void (^)( NSError* _error ) ) _failure;


@end

@implementation CommunicationManager

#pragma mark - Shared Manager
+ (CommunicationManager* ) sharedManager{
    static CommunicationManager *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CommunicationManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    });
    
    return _sharedClient;
}

#pragma mark - Get/Post Request
- ( void ) sendToGetRequest : ( NSString * ) _service
                     params : ( NSDictionary * ) _params
                    success : ( void (^)( id _responseObject ) ) _success
                    failure : ( void (^)( NSError* _error ) ) _failure{
    
    
    [self setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [self setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [self GET:_service parameters:_params success:^(NSURLSessionDataTask *task, id response){
        
//        NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[colorArray dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options:0 error:NULL];
        
        NSLog(@"response = %@", response);
        
        if (_success) {
            _success(response);
        }
        
    }failure:^(NSURLSessionDataTask *task, NSError *error){
        NSLog(@"error = %@", error.description);
        
        if (_failure) {
            _failure(error);
        }
        
    }];
    
}

- ( void ) sendToPostRequest : ( NSString * ) _service
                      params : ( NSDictionary * ) _params
                     success : ( void (^)( id _responseObject ) ) _success
                     failure : ( void (^)( NSError* _error ) ) _failure{
    
    [self setRequestSerializer:[AFHTTPRequestSerializer serializer]];
//    [self setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [self setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [self POST:_service parameters:_params success:^(NSURLSessionDataTask *task, id response){
        
        
        NSString* newStr = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        
        NSLog(@"response string = %@", newStr);
        
        if (_success) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
            _success(dict);
        }
        
    }failure:^(NSURLSessionDataTask *task, NSError *error){
        NSLog(@"error = %@", error.debugDescription);
        
        if (_failure) {
            _failure(error);
        }
        
    }];
    
}

#pragma mark - Fetch Category
- (void)fetchCategory;{
    
    [AppManager sharedManager].arrayCategory = [NSMutableArray array];
    
    PFQuery *query = [PFQuery queryWithClassName:[FACategory parseClassName]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSLog(@"fetched category = %@", objects);
        
        [AppManager sharedManager].arrayCategory = [NSMutableArray arrayWithArray:objects];
        
    }];
    
}

- (void)fetchMine;{
    
    [AppManager sharedManager].arrayLikes = [NSMutableArray array];
    
    PFQuery *query = [PFQuery queryWithClassName:[FALike parseClassName]];
    
    [query whereKey:@"id_user"  equalTo:[User currentUser].objectId];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSLog(@"fetched like pages = %@", objects);
        
        [AppManager sharedManager].arrayLikes = [NSMutableArray arrayWithArray:objects];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:FETCHLIKES_MINE_COMPLETED object:self];
        
    }];
    
}





@end
