//
//  FAOtherViewController.h
//  Faves
//
//  Created by Chen Wang on 4/13/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAOtherViewController : UITableViewController{
    
    IBOutlet UIButton *     btnSettings;
    IBOutlet UIButton *     btnSupport;
    IBOutlet UIButton *     btnHelp;
    
}

- (IBAction)actionSetting:(id)sender;
- (IBAction)actionSupport:(id)sender;
- (IBAction)actionHelp:(id)sender;


@end
