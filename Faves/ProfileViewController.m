//
//  ProfileViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "ProfileViewController.h"

#import <VGParallaxHeader/UIScrollView+VGParallaxHeader.h>
#import <DZNSegmentedControl.h>
#import <YIFullScreenScroll.h>

#import "ProfileHeaderView.h"
#import "ProfilePageViewCell.h"


#define IDENTIFIER_CELL                 @"ProfilePageViewCell"

@interface ProfileViewController ()<DZNSegmentedControlDelegate>

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    [self.tableView registerNib:[UINib nibWithNibName:IDENTIFIER_CELL bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_CELL];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"My Wall"];

    UIBarButtonItem *btnEdit = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(actionEdit:)];
    [self.navigationItem setRightBarButtonItem:btnEdit];
    
    [self.view setBackgroundColor:FlatWhite];
    
    ProfileHeaderView *headerView = [[ProfileHeaderView alloc] init];
    
    [self.tableView setParallaxHeaderView:headerView
                                      mode:VGParallaxHeaderModeFill // For more modes have a look in UIScrollView+VGParallaxHeader.h
                                    height:250];
    
    
    
    
    UILabel *stickyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    stickyLabel.backgroundColor = [UIColor colorWithRed:1 green:0.749 blue:0.976 alpha:1];
    stickyLabel.textAlignment = NSTextAlignmentCenter;
    stickyLabel.text = @"Say hello to Sticky View :)";
    
    NSArray *items = @[@"Faves", @"Likes", @"Photos"];
    
    [[DZNSegmentedControl appearance] setBackgroundColor:[UIColor whiteColor]];
    [[DZNSegmentedControl appearance] setTintColor:UIColorFromRGB(COLOR_BLUE_MAIN)];
    [[DZNSegmentedControl appearance] setHairlineColor:UIColorFromRGB(COLOR_BLUE_MAIN)];
    
    [[DZNSegmentedControl appearance] setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:18.f]];
    [[DZNSegmentedControl appearance] setSelectionIndicatorHeight:2.5f];
    [[DZNSegmentedControl appearance] setAnimationDuration:0.125f];
//    [[DZNSegmentedControl appearance] setWidth:100.f];
    
    
    DZNSegmentedControl *control = [[DZNSegmentedControl alloc] initWithItems:items];
    control.autoAdjustSelectionIndicatorWidth = NO;
    control.delegate = self;
    control.height = 50.f;
    
    control.selectedSegmentIndex = 0;
    [control setCount:@(24) forSegmentAtIndex:0];
    [control setCount:@(45) forSegmentAtIndex:1];
    [control setCount:@(10) forSegmentAtIndex:2];
    
    [control addTarget:self action:@selector(actionSegment:) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.parallaxHeader.stickyViewPosition = VGParallaxHeaderStickyViewPositionBottom; // VGParallaxHeaderStickyViewPositionTop
    [self.tableView.parallaxHeader setStickyView:control
                                      withHeight:control.height];
    
    
    
//    self.fullScreenScroll = [[YIFullScreenScroll alloc] initWithViewController:self scrollView:self.tableView style:YIFullScreenScrollStyleFacebook];
//    self.fullScreenScroll.shouldShowUIBarsOnScrollUp = YES;
//    self.fullScreenScroll.shouldHideNavigationBarOnScroll = NO;
    
}

#pragma mark - Actions
- (void)actionMenu : (id)sender{
    
}

- (void)actionEdit : (id)sender{
    
    [[FBSession activeSession] closeAndClearTokenInformation];
    
//    [self.tabBarController dismissViewControllerAnimated:YES completion:^{
//        NSLog(@"Completion");
//    }];
    
    
}

- (void)actionSegment : (id)sender{
    
    
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // This must be called in order to work
    [self.tableView shouldPositionParallaxHeader];
    
    // scrollView.parallaxHeader.progress - is progress of current scroll
    NSLog(@"Progress: %f", scrollView.parallaxHeader.progress);
    
    // This is how you can implement appearing or disappearing of sticky view
//    [scrollView.parallaxHeader setAlpha:scrollView.parallaxHeader.progress];
}

#pragma mark - UITableview Datasource and Delgate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 10;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ProfilePageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER_CELL forIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
    
}

#pragma mark - UIBarPosition delegate
- (UIBarPosition)positionForBar:(id <UIBarPositioning>)view
{
    return UIBarPositionBottom;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
