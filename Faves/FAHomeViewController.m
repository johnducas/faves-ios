//
//  FAHomeViewController.m
//  Faves
//
//  Created by Chen Wang on 11/03/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAHomeViewController.h"



static const CGFloat ChoosePersonButtonHorizontalPadding        = 60.f;
static const CGFloat ChoosePersonButtonVerticalPadding          = 20.f;

@interface FAHomeViewController ()<UICollectionViewDataSource>

@end

@implementation FAHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    UIImageView *imgViewLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    [imgViewLogo setImage:[UIImage imageNamed:@"logo_unimatch"]];
    [imgViewLogo setContentMode:UIViewContentModeScaleAspectFit];
    [self.navigationItem setTitleView:imgViewLogo];
//    [self.navigationItem.titleView addSubview:imgViewLogo];
    
//    [self.navigationItem.titleView sizeToFit];
    
//    [imgViewLogo sizeToFit];
    
    NSLog(@"titleview = %@", NSStringFromCGRect(self.navigationItem.titleView.frame));
    
    UIBarButtonItem *btnMenu = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(actionMenu:)];
//    [self.navigationItem setRightBarButtonItem:btnMenu];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] init];
    
    
    [self.view setBackgroundColor:FlatWhite];
    
    
    [colList setDataSource:self];
    
    [btnLike setTintColor:UIColorFromRGB(COLOR_LIKE)];
    [btnNope setTintColor:UIColorFromRGB(COLOR_NOPE)];
    
//    [self constructNopeButton];
//    [self constructLikedButton];
    
}

// Create and add the "nope" button.
- (void)constructNopeButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *image = [UIImage imageNamed:@"nope"];
    button.frame = CGRectMake(ChoosePersonButtonHorizontalPadding,
                              0 + ChoosePersonButtonVerticalPadding,
                              image.size.width,
                              image.size.height);
    [button setImage:image forState:UIControlStateNormal];

    
    [button setTintColor:UIColorFromRGB(COLOR_NOPE)];
    
    [button addTarget:self
               action:@selector(nopeFrontCardView)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
}

// Create and add the "like" button.
- (void)constructLikedButton {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *image = [UIImage imageNamed:@"liked"];
    button.frame = CGRectMake(CGRectGetMaxX(self.view.frame) - image.size.width - ChoosePersonButtonHorizontalPadding,
                              CGRectGetMaxY(colList.frame) + ChoosePersonButtonVerticalPadding,
                              image.size.width,
                              image.size.height);
    
    [button setImage:image forState:UIControlStateNormal];
    
    
    [button setTintColor:UIColorFromRGB(COLOR_LIKE)];
    
    [button addTarget:self
               action:@selector(likeFrontCardView)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
}

#pragma mark - Actions
- (void)actionMenu : (id)sender{
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeFilterViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    
    
}

- (IBAction)actionNope:(id)sender;{
    
}

- (IBAction)actionLike:(id)sender;{
    
}


#pragma mark - UICollectionView Datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return  10;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;{
    return 1;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
