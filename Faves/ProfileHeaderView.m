//
//  ProfileHeaderView.m
//  Faves
//
//  Created by Miloslav Sorokin on 24/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "ProfileHeaderView.h"

@implementation ProfileHeaderView

-(id)init{
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    id mainView = [subviewArray objectAtIndex:0];
    
    return mainView;
}

- (void)awakeFromNib{
    
    [_imgProfile.layer setCornerRadius:60];
    
    [_lblName setTextColor:FlatWhite];
    
    [_lblName setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE_LIGHT size:24]];
    [_lblName setShadowColor:FlatWhiteDark];
    [_lblName setShadowOffset:CGSizeMake(1, 1)];
    
//    [_imgBack setImageToBlur:_imgBack.image blurRadius:10 completionBlock:nil];
    
}

- (void)setImageProfile :(UIImage *)_image;{
    if (_image) {
        [_imgBack setImageToBlur:_image blurRadius:20 completionBlock:nil];
        [_imgProfile setImage:_image];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
