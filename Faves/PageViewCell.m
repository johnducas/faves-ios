//
//  PageViewCell.m
//  Faves
//
//  Created by Miloslav Sorokin on 23/12/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "PageViewCell.h"

@implementation PageViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
