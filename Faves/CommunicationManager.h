//
//  CommunicationManager.h
//  Pharmakon
//
//  Created by Yu Li on 4/30/14.
//  Copyright (c) 2014 Pharmakon. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "User.h"
#import "FBPage.h"

#import "FAFriend.h"
#import "FALike.h"
#import "FACategory.h"

@interface CommunicationManager : AFHTTPSessionManager

@property (nonatomic, retain) User *                        me;

+ (CommunicationManager* ) sharedManager;

- (void)fetchCategory;

- (void)fetchMine;


@end
