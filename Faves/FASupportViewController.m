//
//  FASupportViewController.m
//  Faves
//
//  Created by Chen Wang on 4/13/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FASupportViewController.h"

#import <MessageUI/MessageUI.h>

@interface FASupportViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation FASupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    self.navigationItem.title       = @"Support";
    
    self.view.backgroundColor = FlatWhite;
    
}

#pragma mark - Actions

#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 100;
    }
    return 1.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    }
    
    switch (indexPath.section) {
        case 0:{
            cell.textLabel.text = @"Help us Improve";
            break;
        }
        case 1:{
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Privacy";
            }else{
                cell.textLabel.text = @"Terms of service";
            }
            break;
        }
            
        default:
            break;
    }
    
    cell.textLabel.font = [UIFont fontWithName:FONT_HELVETICA_NEUE size:16];
    
    // Configure the cell...
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            if ([MFMailComposeViewController canSendMail])
            {
                
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                [mailer setSubject:@"Feedback"];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"johnducas@unimatch.com", nil];
                [mailer setToRecipients:toRecipients];
                
//                UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
//                NSData *imageData = UIImagePNGRepresentation(myImage);
//                [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
//                
//                NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
//                [mailer setMessageBody:emailBody isHTML:NO];
                
                [self presentViewController:mailer animated:YES completion:nil];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Your device doesn't support the email composer"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            break;
        }
        case 1:{
            
            if (indexPath.row == 0) {
                
                UIViewController *viewController = [UIStoryboard initiateStorybard:STORYBOARD_HOME withViewController:@"FAPrivacyViewController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
            }else if (indexPath.row == 1){
                
                UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FATermsViewController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
            
            break;
        }
            
        default:
            break;
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
