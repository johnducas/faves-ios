//
//  FAFriend.m
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAFriend.h"

@implementation FAFriend
@dynamic id_friend;
@dynamic id_user;


+ (void)load {
    [self registerSubclass];
    
    [[self object] pinInBackground];
}

+ (NSString *)parseClassName{
    return @"Friends";
}

@end
