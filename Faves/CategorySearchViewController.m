//
//  CategorySearchViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/19/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "CategorySearchViewController.h"
#import <YIFullScreenScroll.h>



//#import "SearchFaveCell.h"
#import "SearchPageViewCell.h"

#define IDENTIFIER_CELL                         @"SearchPageViewCell"

@interface CategorySearchViewController ()<UISearchBarDelegate>{
    NSMutableArray *                    arrayList;
    
    UISearchBar *                       searchContent;
}

@end

@implementation CategorySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    [self loadPages];
    
//    [self.collectionView registerNib:[UINib nibWithNibName:IDENTIFIER_CELL bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IDENTIFIER_CELL];
    
    [self.collectionView registerNib:[UINib nibWithNibName:IDENTIFIER_CELL bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:IDENTIFIER_CELL];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
//    [self.navigationItem setTitle:[[AppManager sharedManager].arrayCategoryName objectAtIndex:[self.categoryID integerValue]]];
    
    [self.navigationItem setTitle:self.strCategory];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back"] style:UIBarButtonItemStyleDone target:self action:@selector(actionBack:)];
    [self.navigationItem setLeftBarButtonItem:btnBack];
    
    [self.view setBackgroundColor:FlatWhite];
    [self.collectionView setBackgroundColor:FlatWhite];
    
    self.fullScreenScroll = [[YIFullScreenScroll alloc] initWithViewController:self scrollView:self.collectionView style:YIFullScreenScrollStyleFacebook];
    self.fullScreenScroll.shouldShowUIBarsOnScrollUp = YES;
    self.fullScreenScroll.shouldHideNavigationBarOnScroll = NO;
    
//    self.navigationController.edgesForExtendedLayout = UIRectEdgeTop;
}

#pragma mark - load Pages
- (void)loadPages{
    
    
    
}

#pragma mark - Actions
- (void)actionBack : (id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;{
    return 20;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    SearchPageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IDENTIFIER_CELL forIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.view.frame.size.width / 2 - 10, self.view.frame.size.width / 2 - 10);
    
}

// 3
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 7, 5, 7);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section;{
    return 5;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
