//
//  UIButton+Extension.h
//  Kwibal
//
//  Created by Andrei Sharov on 10/17/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extension)

- (void)underlinedText;

@end
