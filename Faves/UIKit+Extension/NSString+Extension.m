//
//  NSString+Extension.m
//  Chen WangOwner
//
//  Created by Chen Wang on 5/15/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "NSString+Extension.h"
#import <CommonCrypto/CommonDigest.h> 

@implementation NSString (Extension)

#pragma mark - Email Validate -
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ ( NSString* ) timeElapsed : ( NSInteger ) _seconds
{
    NSInteger   index   = 0 ;
    NSInteger   unit    = 0 ;
    NSInteger   units[] = { 1, 60, 3600, 86400, 604800 } ;
    NSArray*    symbols = [ NSArray arrayWithObjects : @"s", @"m", @"h", @"d", @"w", nil ] ;
    
    for( index = 0 ; index < 5 ; index ++ )
    {
        if( _seconds < units[ index ] )
        {
            break ;
        }
    }
    
    unit = floor( _seconds / units[ index - 1 ] ) ;
    return [ NSString stringWithFormat : @"%ld%@", (long)unit, [ symbols objectAtIndex : index ? index - 1 : 0 ] ] ;
}

+ (NSString *)getDateFromNumber : (NSNumber *)year month : (NSNumber *)month day : (NSNumber *)day;{
    
    NSMutableString *strDate = [NSMutableString string];
    
    if (year) {
        [strDate appendString:[year stringValue]];
        
    }
    
    if (month) {
        [strDate appendString:@"-"];
        
        if ([month integerValue] < 10) {
            [strDate appendString:@"0"];
        }
        
        [strDate appendString:[month stringValue]];

    }
    
    if (day) {
        [strDate appendString:@"-"];
        
        if ([day integerValue] < 10) {
            [strDate appendString:@"0"];
        }
        
        [strDate appendString:[day stringValue]];
        
    }
    
    return strDate;
    
}

- (NSNumber *)getYearFromString;
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self];
    
    //now you have the date, you can output the bits you want
    
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *year = [dateFormatter stringFromDate:date];
    
    return [NSNumber numberWithInt:[year intValue]];
}

- (NSNumber *)getMonthFromString;{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self];
    
    //now you have the date, you can output the bits you want
    
    [dateFormatter setDateFormat:@"MM"];
    NSString *year = [dateFormatter stringFromDate:date];
    
    return [NSNumber numberWithInt:[year intValue]];
    
}
- (NSNumber *)getDayFromString;{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self];
    
    //now you have the date, you can output the bits you want
    
    [dateFormatter setDateFormat:@"dd"];
    NSString *year = [dateFormatter stringFromDate:date];
    
    return [NSNumber numberWithInt:[year intValue]];
    
}

- (NSString *)getWeekDayFromString;{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self];
    
    [dateFormatter setDateFormat:@"EEEE"];
    
    return [[dateFormatter stringFromDate:date] substringToIndex:3];
}

- (NSString *)md5
{
    
    NSMutableString *string = [NSMutableString string];
    [string appendString:self];
    [string appendString:@"a1b2c3"];
    
    const char *cStr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
    
}

@end
