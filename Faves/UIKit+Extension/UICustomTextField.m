//
//  UICustomTextField.m
//  GrooveFox
//
//  Created by Miloslav Sorokin on 12/7/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "UICustomTextField.h"

@implementation UICustomTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

@end
