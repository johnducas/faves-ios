//
//  UIStoryboard+Extension.m
//  GrooveFox
//
//  Created by Miloslav Sorokin on 12/7/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "UIStoryboard+Extension.h"

@implementation UIStoryboard (Extension)

+ (UIViewController *)initiateStorybard : (NSString *)storybardName withViewController : (NSString*)controllerName;{
    
    UIViewController *controller = nil;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storybardName bundle:nil];
    
    controller = [storyboard instantiateViewControllerWithIdentifier:controllerName];
    
    return controller;
    
}

@end
