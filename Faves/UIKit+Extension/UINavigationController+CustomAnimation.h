//
//  UINavigationController+CustomAnimation.h
//  Chen WangOwner
//
//  Created by Chen Wang on 5/14/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (CustomAnimation)

- (void)pushViewController : (UIViewController *)viewController
                 animation : (UIViewAnimationTransition)animation;

@end
