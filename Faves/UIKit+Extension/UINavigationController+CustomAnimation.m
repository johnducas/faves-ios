//
//  UINavigationController+CustomAnimation.m
//  Chen WangOwner
//
//  Created by Chen Wang on 5/14/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "UINavigationController+CustomAnimation.h"

@implementation UINavigationController (CustomAnimation)

- (void)pushViewController:(UIViewController *)viewController animation:(UIViewAnimationTransition)animation{
    
    [UIView beginAnimations:nil context:NULL];
    [self pushViewController:viewController animated:NO];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationTransition:animation forView:self.view cache:YES];
    [UIView commitAnimations];
    
}

@end
