//
//  NSString+Extension.h
//  Chen WangOwner
//
//  Created by Chen Wang on 5/15/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (Extension)

+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;
+ ( NSString* ) timeElapsed : ( NSInteger ) _seconds;

+ (NSString *)getDateFromNumber : (NSNumber *)year month : (NSNumber *)month day : (NSNumber *)day;

- (NSNumber *)getYearFromString;
- (NSNumber *)getMonthFromString;
- (NSNumber *)getDayFromString;
- (NSString *)getWeekDayFromString;

- (NSString *)md5;

@end
