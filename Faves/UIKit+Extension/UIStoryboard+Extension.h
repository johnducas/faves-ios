//
//  UIStoryboard+Extension.h
//  GrooveFox
//
//  Created by Miloslav Sorokin on 12/7/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (Extension)

+ (UIViewController *)initiateStorybard : (NSString *)storybardName withViewController : (NSString*)controllerName;

@end
