//
//  UIButton+Extension.m
//  Kwibal
//
//  Created by Andrei Sharov on 10/17/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import "UIButton+Extension.h"

@implementation UIButton (Extension)

- (void)underlinedText;{
    
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
    
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    [commentString addAttribute:NSForegroundColorAttributeName value:[self titleColorForState:UIControlStateNormal] range:NSMakeRange(0, [commentString length])];
    
    [self setAttributedTitle:commentString forState:UIControlStateNormal];
    
}

@end
