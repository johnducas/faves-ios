//
//  UIColor+Hex.h
//  Chen WangOwner
//
//  Created by Chen Wang on 5/14/14.
//  Copyright (c) 2014 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ ( UIColor * )colorWithHexString : ( NSString * )hex;
+ ( UIColor * )colorWithHexString : ( NSString * )hex alpha : (CGFloat)alpha;

@end
