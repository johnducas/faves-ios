//
//  FACategory.m
//  Faves
//
//  Created by Igor Petukhov on 13/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FACategory.h"

@implementation FACategory
@dynamic id_category;
@dynamic name;

+ (void)load {
    [self registerSubclass];
    
    [[self object] pinInBackground];
}

+ (NSString *)parseClassName{
    return @"Category";
}

@end
