//
//  FAUserProfilePhotoViewCell.h
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SwipeView.h>

@interface FAUserProfilePhotoViewCell : UITableViewCell<SwipeViewDataSource, SwipeViewDelegate>{
    
    IBOutlet SwipeView *            viewPhotos;
    
}

+ (instancetype)instantiateFromNib;

@end
