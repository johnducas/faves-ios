//
//  SearchFaveCell.h
//  Faves
//
//  Created by Toki Kagawa on 11/19/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchFaveCell : UITableViewCell{
    
    IBOutlet UIView *               viewContent;
    
    IBOutlet UIImageView *          imgContent;
    IBOutlet UILabel *              lblTitle;
    IBOutlet UILabel *              lblDesc;
    
    IBOutlet UIImageView *          imgViewBack;
    IBOutlet UIButton *             btnAvatar;
    
    IBOutlet UIButton *             btnBookmark;
}

- (IBAction)actionAvatar:(id)sender;
- (IBAction)actionBookmark:(id)sender;

@end
