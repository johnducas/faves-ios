//
//  Faves.h
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Faves : NSObject

@property (nonatomic, strong) NSString *                faveid;
@property (nonatomic, strong) NSString *                userid;
@property (nonatomic, strong) NSString *                title;
@property (nonatomic, strong) NSString *                desc;
@property (nonatomic, strong) NSString *                imgUrl;
@property (nonatomic, strong) NSString *                category;

@end
