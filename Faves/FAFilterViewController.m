//
//  FAFilterViewController.m
//  Faves
//
//  Created by Chen Wang on 19/01/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FAFilterViewController.h"

@interface FAFilterViewController ()

@end

@implementation FAFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - load Controller
- (void)loadController{
    
    [self.navigationItem setTitle:@"Filter"];
    
    UIBarButtonItem *btnSave = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(actionSave:)];
    [self.navigationItem setRightBarButtonItem:btnSave];
    
    
    [self.view setBackgroundColor:FlatWhite];
    
}

#pragma mark - Actions
- (void)actionSave : (id)sender{
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    switch (section) {
        case 0:{
            return 2;
            break;
        }
        case 1:{
            return 1;
            
            break;
        }
        case 2:{
            return 1;
            break;
        }
            
        default:
            return 0;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    }
    
    NSString *strText;
    switch (indexPath.section) {
        case 0:{
            
            switch (indexPath.row) {
                case 0:{
                    strText = @"Guys";
                    
                    break;
                }
                case 1:{
                    strText = @"Girls";
                    break;
                }
                    
                default:
                    break;
            }
            break;
        }
        case 1:{
            strText = @"Guys";
            break;
        }
        case 2:{
            strText = @"Guys";
            break;
        }
            
        default:
            break;
    }
    
    cell.textLabel.text = strText;
    [cell.textLabel setFont:[UIFont fontWithName:FONT_HELVETICA_NEUE size:16]];
    [cell.textLabel setTextColor:FlatBlack];
    
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
