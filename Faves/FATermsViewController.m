//
//  FATermsViewController.m
//  Faves
//
//  Created by Chen Wang on 4/14/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import "FATermsViewController.h"

@interface FATermsViewController ()

@end

@implementation FATermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    self.navigationItem.title       =  @"Terms of Service";
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
