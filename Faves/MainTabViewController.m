//
//  MainTabViewController.m
//  Faves
//
//  Created by Toki Kagawa on 11/18/26 H.
//  Copyright (c) 26 Heisei Chen Wang. All rights reserved.
//

#import "MainTabViewController.h"

@interface MainTabViewController (){
    
    NSInteger               previousTabIndex;
    NSInteger               currentTabIndex;
    
}

@end

@implementation MainTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadController];
    
    [self loadTabsUnimatch];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionLoggedOut:) name:FB_LOG_OUT_NOTIFICATON object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load Controller
- (void)loadController{
    
    
    [self.tabBar setBarStyle:UIBarStyleDefault];
//    [self.tabBar setTintColor:FlatBlueDark];
    [self.tabBar setTintColor:UIColorFromRGB(COLOR_BLUE_UNIMATCH)];
    [self.tabBar setTranslucent:YES];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:FONT_HELVETICA_NEUE size:12], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(COLOR_BLUE_MAIN), NSForegroundColorAttributeName, nil] forState:UIControlStateHighlighted];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:UIColorFromRGB(COLOR_BLUE_MAIN), NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
//    [[UITabBarItem appearance] ]

    [self setDelegate:self];
    
    
    previousTabIndex = 0;
    currentTabIndex = 0;
    
}

- (void)loadTabs{
    
    UIViewController *viewControllerHome = [UIStoryboard initiateStorybard:STORYBOARD_HOME withViewController:@"MainNavController"];
    UIViewController *viewControllerSearch = [UIStoryboard initiateStorybard:STORYBOARD_SEARCH withViewController:@"MainNavController"];
    UIViewController *viewControllerMessage = [UIStoryboard initiateStorybard:STORYBOARD_INBOX withViewController:@"MainNavController"];
    UIViewController *viewControllerProfile = [UIStoryboard initiateStorybard:STORYBOARD_MYPROFILE withViewController:@"MainNavController"];
    
    [self setViewControllers:@[viewControllerHome, viewControllerSearch, viewControllerMessage, viewControllerProfile]];
    
    
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:3];
    
    
    item1.title = @"Home";
    item2.title = @"Search";
    item3.title = @"Chat";
    item4.title = @"Profile";
    
    [item1 setImage:[UIImage imageNamed:@"icon_favorite"] ];
    [item2 setImage:[UIImage imageNamed:@"icon_search1"] ];
    [item3 setImage:[UIImage imageNamed:@"icon_chat"] ];
    [item4 setImage:[UIImage imageNamed:@"icon_profile"] ];
    
    
}

- (void)loadTabsUnimatch{
    
    UIViewController *viewControllerHome = [UIStoryboard initiateStorybard:STORYBOARD_HOME withViewController:@"MainNavController"];
//    UIViewController *viewControllerSearch = [UIStoryboard initiateStorybard:STORYBOARD_SEARCH withViewController:@"MainNavController"];
//    UIViewController *viewControllerMessage = [UIStoryboard initiateStorybard:STORYBOARD_INBOX withViewController:@"MainNavController"];
    UIViewController *viewControllerMatches = [UIStoryboard initiateStorybard:STORYBOARD_MATCHES withViewController:@"MainNavController"];
//    UIViewController *viewControllerProfile = [UIStoryboard initiateStorybard:STORYBOARD_MYPROFILE withViewController:@"MainNavController"];
    UIViewController *viewControllerOther = [UIStoryboard initiateStorybard:STORYBOARD_OTHER withViewController:@"MainNavController"];
    
    [self setViewControllers:@[viewControllerHome, viewControllerMatches, viewControllerOther]];
    
    
    UITabBarItem *item1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *item2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [self.tabBar.items objectAtIndex:2];
//    UITabBarItem *item4 = [self.tabBar.items objectAtIndex:3];
    
    
    item1.title = @"Home";
    item2.title = @"Matches";
    item3.title = @"Other";
//    item4.title = @"Profile";
    
    [item1 setImage:[UIImage imageNamed:@"icon_home_unimatch"] ];
    [item2 setImage:[UIImage imageNamed:@"icon_star_unimatch"] ];
    [item3 setImage:[UIImage imageNamed:@"icon_threedots"] ];
//    [item4 setImage:[UIImage imageNamed:@"icon_profile"] ];
    
    
}

#pragma mark - Actions
- (void)actionCloseCreate : (id)sender{
    
    [self setSelectedIndex:previousTabIndex];
    
    [self setTabBarVisible:YES animated:YES];
    
}

- (void)actionLoggedOut : (id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - UITabbar Delegate
- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    NSLog(@"Tab index = %lu", (unsigned long)indexOfTab);

    if (currentTabIndex != indexOfTab) {
        previousTabIndex = currentTabIndex;
        currentTabIndex = indexOfTab;
    }
    
    if (currentTabIndex == 2) {
//        [self setTabBarVisible:NO animated:YES];
    }

}

- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return;
    
    // get a frame calculation ready
    CGRect frame = self.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.2 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    }];
    
}

- (BOOL)tabBarIsVisible {
    return self.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
