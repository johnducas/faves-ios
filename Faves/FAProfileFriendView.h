//
//  FAProfileFriendView.h
//  Faves
//
//  Created by Chen Wang on 3/19/15.
//  Copyright (c) 2015 Chen Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAProfileFriendView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *      imgProfile;
@property (nonatomic, weak) IBOutlet UILabel *          lblName;

+ (instancetype)instantiateFromNib;

@end
